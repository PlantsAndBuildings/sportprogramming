#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;

bool operator<(const string &s1,const string &s2){
	ll i=0;
	while(i<s1.length() && i<s2.length() && s1[i] == s2[i]) i++;
	if(i == s1.length()) return true;
	if(i == s2.length()) return false;
	return s1[i]<s2[i];
}

bool cmpfn(const pair<string,ll> & p1, const pair<string,ll> & p2){
	if(p1.second != p2.second) return p1.second > p2.second;
	return p1.first < p2.first;
}

int main(int argc, char ** argv){
	string tmp;
	ll age;
	vector< pair<string, ll> > v;
	while(cin>>tmp){
		cin>>age;
		v.push_back(pair<string,ll>(tmp,age));
//		cout<<"poops\n";
	}
//	cout<<"poopsend\n";
	sort(v.begin(),v.end(), cmpfn);
	for(ll i=0; i<v.size(); i++){
		cout<<v[i].first<<"\n";
	}
	return 0;
}
