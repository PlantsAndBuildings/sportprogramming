// {S} is stable
// SS is stable
// Replace { by } or vice versa
// L(i,j) denote the min no of operations to stabilize s[i..j] i < j
// if(s[i] == { and s[j] == } and (j-i+1)%2==0) then min(L(i+1,j-1)+0/1/2,L(i,k)+L(k+1,j)) over all valid k. Note that k == i and k == j are valid ks.
// L(i,i+1) = 0 if s[i] = { and s[i+1] = }
// L(i,i+1) = 2 if s[i] = } and s[i+1] = {
// L(i,i+1) = 1 otherwise

// Simple recursive solution

#include <iostream>
#include <vector>
#include <string>

using std::cin;
using std::cout;
using std::string;

const int inf = 1<<31 - 1;

int min(int a, int b){
	return a>b?b:a;
}

int solve(string & s, int beg, int end){
	if(beg >= end){
		return 0;
	}
	if(beg+1 == end){
		if(s[beg] == '{' && s[end] == '}'){
			return 0;
		}else if(s[beg] == '}' && s[end] == '{'){
			return 2;
		}else{
			return 1;
		}
	}else{
		int ans = inf;
		for(int i=beg+1; i<end-1; i+=2){
			ans = min(ans,solve(s,beg,i)+solve(s,i+1,end));
		}
		int tmp;
		if(s[beg] == '{' && s[end] == '}'){
			tmp = 0;
		}else if(s[beg] == '}' && s[end] == '{'){
			tmp = 2;
		}else{
			tmp = 1;
		}
		ans = min(ans,tmp+solve(s,beg+1,end-1));
		return ans;
	}
}

int main(int argc, char ** argv){
	while(1){
		string s;
		cin>>s;
		if(s[0] == '-') break;
		int ans = inf;	
		ans = solve(s,0,s.length()-1);
		cout<<ans<<"\n";
	}
	return 0;
}
