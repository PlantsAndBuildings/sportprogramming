//https://www.codechef.com/problems/TREEROOT
#include <iostream>
#include <vector>
#include <cstdlib>
#include <utility>
#include <cstdio>
#include <fstream>


using namespace std;

typedef long long int ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ll t;
	//fstream fin,fout;
	//fin.open("in",ios::in);
	//fout.open("out",ios::out);
	scanf("%lld",&t);
	//fin>>t;
	while(t--){
		ll n,id,sum_c;
		ll sum_of_ids = 0;
		ll sum_of_sum_c = 0;
		scanf("%lld",&n);
		//fin>>n;
		for(ll i =0; i<n; i++){
			scanf("%lld%lld",&id,&sum_c);
			//fin>>id>>sum_c;
			sum_of_ids += id;
			sum_of_sum_c += sum_c;
		}
		ll root = sum_of_ids - sum_of_sum_c;
		if(root > 0){
			printf("%lld\n",root);
		//	fout<<root<<"\n";
		}

	}
	
	return 0;
}
