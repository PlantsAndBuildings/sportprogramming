#include <iostream>
#include <vector>

using namespace std;

const int inf = 1<<31 - 1;

int min(int a, int b){
	return a>b?b:a;
}

int main(int argc, char ** argv){
	int ctr = 1;
	while(1){
		string s;
		cin>>s;
		if(s[0] == '-') break;
		int n = s.length();
		vector< vector<int> > dp(n,vector<int> (n,inf));
		for(int i=0; i<n-1; i++){
			if(s[i] == '{' && s[i+1] == '}'){
				dp[i][i+1] = 0;
			}else if(s[i] == '}' && s[i+1] == '{'){
				dp[i][i+1] = 2;
			}else{
				dp[i][i+1] = 1;
			}
		}
		for(int len = 4; len <= n; len += 2){
			for(int i=0; i<n; i++){
				if(i+len-1 < n){
					dp[i][i+len-1] = inf;
					for(int k=2; k<len; k+=2){
						dp[i][i+len-1] = 
							min(
							dp[i][i+k-1]+dp[i+k][i+len-1],
							dp[i][i+len-1]
							);
					}
					if(s[i] == '{' && s[i+len-1] == '}'){
						dp[i][i+len-1] = min(dp[i][i+len-1],dp[i+1][i+len-2]);
					}else if(s[i] == '}' && s[i+len-1] == '{'){
						dp[i][i+len-1] = min(dp[i][i+len-1],dp[i+1][i+len-2]+2);
					}else{
						dp[i][i+len-1] = min(dp[i][i+len-1],dp[i+1][i+len-2]+1);
					}
				}
			}
		}
		cout<<ctr<<". "<<dp[0][n-1]<<"\n";
		ctr++;
	}
	return 0;
}
