#include <iostream>
#include <vector>
#include <stack>


using namespace std;

typedef long long int ll;

bool palin(ll num){
	ll len = 0;
	stack<ll> s;
	ll k = 1;
	while((num/k)!=0){
		k *= 10;
		len ++;
	}
	if(len == 1){
		return true;
	}
	for(int i=0; i<len/2; i++){
		s.push(num%10);
		num = num/10;
	}
	if(len%2 == 1){
		num /= 10;
	}
	while(num != 0){
		if(num%10 != s.top()){
			return false;
		}
		s.pop();
		num /= 10;
	}
	return true;

}

int main(int argc, char ** argv){
	/*
	cout<<palin(10101)<<"\n";
	cout<<palin(33)<<"\n";
	cout<<palin(54)<<"\n";
	*/
	ll n;
	cin>>n;
	vector<ll> pp;
	vector<bool> prime(1005000, true);
	prime[0] = false;
	prime[1] = false;
	for(int i=2; i<1005000; i++){
		if(prime[i]){
			for(int j=2*i; j<1005000; j += i){
				prime[j] = false;
			}
		}
	}
	for(int i=0; i<1005000; i++){
		if(prime[i]){
			if(palin(i)){
				pp.push_back(i);
			}
		}
	}
	int k =0 ;
	while(pp[k] < n){
		k++;
	}
	cout<<pp[k]<<"\n";
	return 0;
}
