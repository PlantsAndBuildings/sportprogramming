//https://www.hackerrank.com/contests/june-world-codesprint/challenges/johnland

#include "stdio.h"
#include "string.h"
#include "vector"
#include "algorithm"
#include <iostream>

using namespace std;

#define pii pair<int, int>
#define pip pair<int, pii>
#define F first
#define S second

inline void inp(int *n)
{
    *n = 0;
    int ch = getchar_unlocked();
    int sign = 1;
    while(ch < '0' || ch > '9') 
    {
        if (ch == '-') 
            sign = -1;
        ch = getchar_unlocked();
    }
    while(ch >= '0' && ch <= '9')
        (*n) = ((*n)<<3) + ((*n)<<1) + ch - '0', ch = getchar_unlocked();
    *n = (*n)*sign;
}

const int MAX = 100010;

class Union_Find
{
  public:

	int id[MAX], sz[MAX];
	Union_Find(int n)	
	{
		for (int i = 0; i < n; ++i)
		{
			id[i] = i;
			sz[i] = 1;
		}
	}
	
	int root(int i)
	{
		while(i != id[i])
		{
			id[i] = id[id[i]];	
			i = id[i];
		}
		return i;
	}
	int find(int p, int q)
	{
		return root(p)==root(q);
	}
	void unite(int p, int q)
	{
		int i = root(p);
		int j = root(q);

		if(sz[i] < sz[j])
		{
			id[i] = j;
			sz[j] += sz[i];
		}
		else
		{
			id[j] = i;
			sz[i] += sz[j];
		}
	}
};


vector< pip > graph;
int n, e;
long long int T;
vector< vector<pii> > MST;
vector<bool> vis;

void Kruskal_MST()
{
	Union_Find UF(n);
	int u, v;

	for (int i = 0; i < e; ++i)
	{
		u = graph[i].S.F;
		v = graph[i].S.S;
		if( !UF.find(u, v) )
		{
			UF.unite(u, v);
			MST[u].push_back(pii(v,graph[i].F));
			MST[v].push_back(pii(u,graph[i].F));
			T += graph[i].F;
		}
	}
}

void dfs(vector< vector<pii> > & al, vector<long long> & deg_subtree, vector<bool> & vis, long long root){
	
	vis[root] = true;
	deg_subtree[root] = 1;
	if(al[root].empty()){
		return ;
	}
	if(!al[root].empty()){
		for(int i=0; i<al[root].size(); i++){
			if(!vis[al[root][i].F]){
				dfs(al,deg_subtree,vis,al[root][i].F);
				deg_subtree[root] += deg_subtree[al[root][i].F];
			}
		}
	}
}

void dfs2(vector< vector<pii> > & al, vector < long long > & deg_subtree, vector <long long> & cnt, vector<bool> & vis, long long root){
	vis[root] = true;
	if(!al[root].empty()){
		for(int i=0; i<al[root].size(); i++){
			if(!vis[al[root][i].F]){
				cnt[al[root][i].S] += (deg_subtree[0]-deg_subtree[al[root][i].F])*deg_subtree[al[root][i].F];
				dfs2(al,deg_subtree,cnt,vis,al[root][i].F);
			}
		}
	}
}

int main()
{
	int u, v, c;
	inp(&n);	
	inp(&e);
	
	graph.resize(e);
	MST.resize(n);


	for (int i = 0; i < e; ++i)
	{
		inp(&u);	
		inp(&v);
		inp(&c);	
		u--;
		v--;
		graph[i] = pip( c, pii(u,v));
	}
	sort(graph.begin(), graph.end());	

	T = 0;
	Kruskal_MST();
	vector<long long> cnt(e*4,0);
	vector<long long> deg_subtree(n,0);
	
	vector<bool> vis(n,false);
	dfs(MST,deg_subtree,vis,0);
	for(int i=0; i<n; i++) vis[i] = false;
	dfs2(MST,deg_subtree,cnt,vis,0);
	for(int i =0; i<((4*e)-1); i++){
		cnt[i+1] += cnt[i]/2;
		cnt[i] = cnt[i]%2;
	}
	int i = 4*e-1;
	while(cnt[i] != 1) i--;
	while(i!=-1){
		cout<<cnt[i];
		i--;
	}
	cout<<"\n";
	return 0;
}
