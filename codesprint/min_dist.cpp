//https://www.hackerrank.com/contests/june-world-codesprint/challenges/minimum-distances
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <set>
#include <map>
#include <string>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;

int main(int argc, char ** argv){

	ll n,tmp;
	cin>>n;
	vi cnt(100005,0);
	vi lo(100005,-1);
	ll mindiff = 10005;
	for(int i=0; i<n; i++){
		cin>>tmp;
		cnt[tmp]++;
		if(lo[tmp] == -1){
			lo[tmp] = i;
		}else{
			if((i - lo[tmp])<mindiff){
				mindiff = i-lo[tmp];
			}
			lo[tmp] = i;
		}
	}
	if(mindiff == 10005){
		cout<<"-1\n";
	}else{
		cout<<mindiff<<"\n";
	}

	return 0;
}
