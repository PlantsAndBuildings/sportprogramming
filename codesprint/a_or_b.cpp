//https://www.hackerrank.com/contests/june-world-codesprint/challenges/aorb

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstring>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;

int main(int argc, char ** argv){
	ll q;
	cin>>q;
	char * ref[] = {
		"0000",
		"0001",
		"0010",
		"0011",
		"0100",
		"0101",
		"0110",
		"0111",
		"1000",
		"1001",
		"1010",
		"1011",
		"1100",
		"1101",
		"1110",
		"1111"		
	};
	while(q--){
		ll k;
		char *a,*b,*c,*bina,*binb, *binc;
		ll lena, lenb, lenc;
		a = (char*)calloc(50010,sizeof(char));
		b = (char*)calloc(50010,sizeof(char));
		c = (char*)calloc(50010,sizeof(char));
		cin>>k;
		cin>>a>>b>>c;

		bina = (char*)calloc(200010,sizeof(char));
		binb = (char*)calloc(200010,sizeof(char));
		binc = (char*)calloc(200010,sizeof(char));
		int i = 0;
		for(i=0; a[i] != '\0'; i++){
			if(a[i]<='9' && a[i]>='0'){
				bina[4*i] = ref[a[i]-'0'][0];
				bina[4*i+1] = ref[a[i]-'0'][1];
				bina[4*i+2] = ref[a[i]-'0'][2];
				bina[4*i+3] = ref[a[i]-'0'][3];
			}else if(a[i]>='A' && a[i]<='F'){
				bina[4*i] = ref[a[i]-'A'+10][0];
				bina[4*i+1] = ref[a[i]-'A'+10][1];
				bina[4*i+2] = ref[a[i]-'A'+10][2];
				bina[4*i+3] = ref[a[i]-'A'+10][3];
			}
		}
		for(int j=0; j<(2*i+2); j++){
			char tmp = bina[j];
			bina[j] = bina[4*i+3-j];
			bina[4*i+3-j] = bina[j];
		}
		bina[4*i+4] = '\0';
		cout<<bina<<"\n";
		for(i=0; b[i] != '\0'; i++){
			if(b[i]<='9' && b[i]>='0'){
				binb[4*i] = ref[b[i]-'0'][0];
				binb[4*i+1] = ref[b[i]-'0'][1];
				binb[4*i+2] = ref[b[i]-'0'][2];
				binb[4*i+3] = ref[b[i]-'0'][3];
			}else if(b[i]>='A' && b[i]<='F'){
				binb[4*i] = ref[b[i]-'A'+10][0];
				binb[4*i+1] = ref[b[i]-'A'+10][1];
				binb[4*i+2] = ref[b[i]-'A'+10][2];
				binb[4*i+3] = ref[b[i]-'A'+10][3];
			}
		}
		for(int j=0; j<(2*i+2); j++){
			char tmp = binb[j];
			binb[j] = binb[4*i+3-j];
			binb[4*i+3-j] = binb[j];
		}
		binb[4*i+4] = '\0';
		cout<<binb<<"\n";
		for(i=0; c[i] != '\0'; i++){
			if(c[i]<='9' && c[i]>='0'){
				binc[4*i] = ref[c[i]-'0'][0];
				binc[4*i+1] = ref[c[i]-'0'][1];
				binc[4*i+2] = ref[c[i]-'0'][2];
				binc[4*i+3] = ref[c[i]-'0'][3];
			}else if(c[i]>='A' && c[i]<='F'){
				binc[4*i] = ref[c[i]-'A'+10][0];
				binc[4*i+1] = ref[c[i]-'A'+10][1];
				binc[4*i+2] = ref[c[i]-'A'+10][2];
				binc[4*i+3] = ref[c[i]-'A'+10][3];
			}
		}
		for(int j=0; j<(2*i+2); j++){
			char tmp = binc[j];
			binc[j] = binc[4*i+3-j];
			binc[4*i+3-j] = binc[j];
		}
		binc[4*i+4] = '\0';
		cout<<binc<<"\n";
/*
		ll num_bits_changed = 0;
		for(int i=0; binc[i] != '\0'; i++){
			if(binc[i] == '0'){
			
					if(bina[i] == '1'){
						bina[i] = '0';
						num_bits_changed++;
					}
			
		
					if(binb[i] == '1'){
						binb[i] = '0';
						num_bits_changed++;
					}
				
			}else if(binc[i] == '1'){
				if(bina[i] == '1' || binb[i] == '1');
				else{
					binb[i] = '1';
					num_bits_changed++;
				}
		}
		if(num_bits_changed > k){
 			cout<<"-1\n";
			continue;
		}
		for(int i=200000; i
*/		

	}
	return 0;
}
