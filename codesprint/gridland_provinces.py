def solve(pr0,pr1):
    strset = set()
    cw = pr0 + pr1[::-1]
    ccw = pr1 + pr0[::-1]
    zz0 = ""
    zz1 = ""
    n = len(pr0)
    for i in range(len(pr0)):
        if i%2 == 0:
            zz0 = zz0 + pr0[i] + pr1[i]
            zz1 = zz1 + pr1[i] + pr0[i]
        else:
            zz0 = zz0 + pr1[i] + pr0[i]
            zz1 = zz1 + pr0[i] + pr1[i]
    # top left
    strset.add(cw)
    strset.add(cw[::-1])
    strset.add(zz0)
    strset.add(zz0[::-1])
    strset.add(ccw[2*n-1] + ccw[0:2*n-1])
    strset.add((ccw[2*n-1] + ccw[0:2*n-1])[::-1])
    
    # bottom left
    strset.add(ccw)
    strset.add(ccw[::-1])
    strset.add(zz1)
    strset.add(zz1[::-1])
    strset.add(cw[2*n-1]+cw[0:2*n-1])
    strset.add((cw[2*n-1]+cw[0:2*n-1])[::-1])
    
    # top right
    strset.add(ccw[n:]+ccw[0:n])
    strset.add((ccw[n:]+ccw[0:n])[::-1])
    if n%2 == 0:
        strset.add(zz0[::-1])
    else:
        strset.add(zz1[::-1])
    strset.add(cw[n-1:]+cw[0:n-1])
    strset.add((cw[n-1:]+cw[0:n-1])[::-1])
    
    
    # bottom right
    if n%2 == 0:
        strset.add(zz1[::-1])
    else:
        strset.add(zz0[::-1])
    strset.add(cw[n:]+cw[0:n])
    strset.add((cw[n:]+cw[0:n])[::-1])
    strset.add(ccw[n-1:]+ccw[0:n-1])
    strset.add((ccw[n-1:]+ccw[0:n-1])[::-1])

    for i in range(1,n-1):
        # top
        strset.add(cw[i:]+cw[0:i])
        strset.add((cw[i:]+cw[0:i])[::-1])
        strset.add(ccw[2*n-i-1:]+ccw[0:2*n-i-1])
        strset.add((ccw[2*n-i-1:]+ccw[0:2*n-i-1])[::-1])
        if i%2 == 0:
            strset.add(cw[i:2*n-i] + zz1[2*(i-1)+1::-1])
            strset.add((cw[i:2*n-i] + zz1[2*(i-1)+1::-1])[::-1])
            strset.add(ccw[2*n-i-1:]+ccw[0:i+1] + zz0[2*(i+1):])
            strset.add((ccw[2*n-i-1:]+ccw[0:i+1] + zz0[2*(i+1):])[::-1])
        else:
            strset.add(cw[i:2*n-i] + zz0[2*(i-1)+1::-1])
            strset.add(ccw[2*n-i-1:]+ccw[0:i+1] + zz1[2*(i+1):])
            strset.add((cw[i:2*n-i] + zz0[2*(i-1)+1::-1])[::-1])
            strset.add((ccw[2*n-i-1:]+ccw[0:i+1] + zz1[2*(i+1):])[::-1])
        
        # bottom
        strset.add(ccw[i:]+ccw[0:i])
        strset.add((ccw[i:]+ccw[0:i])[::-1])
        strset.add(cw[2*n-i-1:]+cw[0:2*n-i-1])
        strset.add((cw[2*n-i-1:]+cw[0:2*n-i-1])[::-1])
        if i%2 == 0:
            strset.add(ccw[i:2*n-i] + zz0[2*(i-1)+1::-1])
            strset.add((ccw[i:2*n-i] + zz0[2*(i-1)+1::-1])[::-1])
            strset.add(cw[2*n-i-1:]+cw[0:i+1] + zz1[2*(i+1):])
            strset.add((cw[2*n-i-1:]+cw[0:i+1] + zz1[2*(i+1):])[::-1])
        else:
            strset.add(ccw[i:2*n-i] + zz1[2*(i-1)+1::-1])
            strset.add((ccw[i:2*n-i] + zz1[2*(i-1)+1::-1])[::-1])
            strset.add(cw[2*n-i-1:]+cw[0:i+1] + zz0[2*(i+1):])
            strset.add((cw[2*n-i-1:]+cw[0:i+1] + zz0[2*(i+1):])[::-1])
    for elem in strset:
        print elem
    return len(strset)

q = int(raw_input())
while q > 0:
    n = int(raw_input())
    pr0 = raw_input()
    pr1 = raw_input()
    print solve(pr0,pr1)
    q -= 1
