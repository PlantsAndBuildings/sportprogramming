//https://www.hackerrank.com/contests/june-world-codesprint/challenges/equal-stacks
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

int main(int argc, char ** argv){
	ll n1, n2, n3;
	cin>>n1>>n2>>n3;
	vvi h(3,vi(100005,0));
	
	for(int i=0; i<n1; i++){
		cin>>h[0][i];
	}
	for(int i=0; i<n2; i++){
		cin>>h[1][i];
	}
	for(int i=0; i<n3; i++){
		cin>>h[2][i];
	}

	vi cnt(10000005,0);
	cnt[h[0][n1-1]]++;
	for(int i=n1-1; i>0; i--){
		h[0][i-1] = h[0][i-1]+h[0][i];
		cnt[h[0][i-1]]++;
	}
	cnt[h[1][n2-1]]++;
	for(int i=n2-1; i>0; i--){
		h[1][i-1] = h[1][i-1]+h[1][i];
		cnt[h[1][i-1]]++;
	}
	cnt[h[2][n3-1]]++;
	for(int i=n3-1; i>0; i--){
		h[2][i-1] = h[2][i-1]+h[2][i];
		cnt[h[2][i-1]]++;
	}

	for(int i=10000000; i>=0; i--){
		if(cnt[i] == 3){
			cout<<i<<"\n";
			return 0;
		}
	}
	cout<<"0\n";
	/*
	for(int i=0; i<n1; i++){
		cout<<h[0][i]<<" ";
	}
	cout<<"\n";
	for(int i=0; i<n2; i++){
		cout<<h[1][i]<<" ";
	}
	cout<<"\n";
	for(int i=0; i<n3; i++){
		cout<<h[2][i]<<" ";
	}
	cout<<"\n";
	*/


	return 0;
}

