// Codeforces round 377 div 2

/*
 * This code is awesome
 * Wednesday 26 October 2016 09:23:41 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <map>

#define fs first
#define sc second
#define all(c) c.begin(),c.end()

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

const ll inf = 1000000000+9;

int main(int argc, char ** argv){
	ll n, k;
	cin>>n>>k;
	vi a(n,0), b(n,0);
	ll num = 0;
	for(int i=0; i<n; i++) cin>>a[i];
	b[0] = a[0];
	for(int i=1; i<n; i++){
		if(a[i]+b[i-1] < k){
			b[i] = a[i]+(k-(a[i]+b[i-1]));
			num += b[i]-a[i];
		}else{
			b[i] = a[i];
		}
	}
	cout<<num<<"\n";
	for(int i=0; i<n; i++) cout<<b[i]<<" ";
	cout<<"\n";
	return 0;
}
