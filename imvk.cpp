#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cstdio>

using namespace std;



int main(int argc, char ** argv){
	int t;
	cin>>t;
	while(t--){
		string n;
		cin>>n;
		bool div5 = false, div9 = false, div11 = false;
		if(n[n.length()-1] == '0' || n[n.length()-1] == '5') div5 = true;
		int sumodd = 0, sumeven = 0;
		for(int i=0; i<n.length(); i++){
			if(i%2 == 0) sumeven += (int)(n[i]-'0');
			else sumodd += (int)(n[i]-'0');
		}
		//cout<<sumodd<<" "<<sumeven<<"\n";
		if((sumodd+sumeven)%9==0)div9 = true;
		if(abs(sumodd-sumeven)%11 == 0) div11 = true;
		if(div5 && div9 && div11) cout<<"yes\n";
		else cout<<"no\n";
	}
	return 0;
}
