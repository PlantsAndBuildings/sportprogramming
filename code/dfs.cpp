#include <iostream>
#include <vector>

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

class Graph{
	private:
		ll V;
		vvi al;
		bool isDigraph;		// If the graph is directed this boolean is set to true.
	public:
		Graph(ll);
		Graph(ll, bool);
		void addEdge(ll, ll);
		void DFS();
		void DFSUtil(vvi &, vector<bool> &, ll);
		void printGraph();
};

Graph::Graph(ll V){
	this->V = V;
	(this->al).resize(V);
	this->isDigraph = false;
}

Graph::Graph(ll V, bool isDigraph){
	this->V = V;
	(this->al).resize(V);
	this->isDigraph = isDigraph;
}

void Graph::addEdge(ll u, ll v){
	if(isDigraph){
		(this->al)[u].push_back(v);
	}else{
		(this->al)[u].push_back(v);
		(this->al)[v].push_back(u);
	}
	return;
}

void Graph::DFS(){
	vector<bool> visited(V, false);
	for(ll i=0; i<V; i++){
		if(!visited[i]) DFSUtil(al,visited,i);
	}
}

void Graph::DFSUtil(vvi & al, vector<bool> & visited, ll source){
	visited[source] = true;
	cout<<source<<endl;
	for(vi::iterator viIter = al[source].begin(); viIter != al[source].end(); viIter++){
		if(!visited[*viIter]){
			DFSUtil(al,visited,*viIter);
		}
	}
}

void Graph::printGraph(){
	ll v_no = 0; 
	for(vvi::iterator vviIter = al.begin(); vviIter != al.end(); vviIter++, v_no++){
		cout<<v_no<<"->";
		for(vi::iterator viIter = (*vviIter).begin(); viIter != (*vviIter).end(); viIter++){
			cout<<(*viIter)<<" ";
		}
		cout<<endl;
	}
}

int main(int argc, char ** argv){
	ll v,e;
	cin>>v>>e;
	Graph g1(v,true);
	Graph g2(v);
	for(ll i=0; i<e; i++){
		ll tmp1, tmp2;
		cin>>tmp1>>tmp2;
		tmp1--; tmp2--;
		g1.addEdge(tmp1,tmp2);
		g2.addEdge(tmp1,tmp2);
	}
	cout<<"Directed Graph:\n";
	g1.printGraph();
	cout<<"Undirected Graph:\n";
	g2.printGraph();
	cout<<"DFS of directed graph\n";
	g1.DFS();
	cout<<"DFS of undirected graph\n";
	g2.DFS();
	/*
	Graph g(3,true);
	g.addEdge(0,1);
	g.addEdge(1,2);
	g.addEdge(2,0);
	g.printGraph();
	g.DFS();
	*/
	return 0;
}
