// http://www.spoj.com/problems/AGGRCOW/
// Nice Problem on Binary Search -- AC

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;


ll check(vi & x, ll mindis, ll c){
	if(mindis == 0){
		return 1;
	}
	vector<bool> isCow(x.size(),false);
	ll lastCow = -1;
	ll numCows = 0;
	for(ll i=0; i<x.size(); i++){
		if(i==0){
			isCow[i] = true;
			lastCow = 0;
			numCows++;
		}else{
			if((x[i]	- x[lastCow]) >= mindis){
				isCow[i] = true;
				lastCow = i;
				numCows++;
			}
		}
	}
	if(numCows >= c) return 1;
	else return 0;
}

ll binSearch(vi & x, ll c, ll beg, ll end){
	//cout<<"beg = "<<beg<<"\nend = "<<end<<"\n";
	if(beg+1  == end){
		return beg;
	}
	ll mid = (beg+end)/2;
	if(check(x,mid,c) == 1){
		//cout<<"ch["<<mid<<"] = 1\n";
		return binSearch(x,c,mid,end);
	}else if (check(x,mid,c) == 0){
		//cout<<"ch["<<mid<<"] = 0\n";
		return binSearch(x,c,beg,mid);
	}
}

ll solve(vi & x, ll c){
	ll beg = 0;
	ll end = x[x.size()-1];
	if(check(x,end,c) == 1){
		return end;
	}else{
		return binSearch(x,c,beg,end);
	}
}

int main(int argc, char ** argv){
	ll t;
	cin>>t;
	while(t--){
		ll n,c;
		cin>>n>>c;
		vi x(n,0);
		for(ll i=0; i<n; i++){
			cin>>x[i];
		}
		sort(x.begin(), x.end());
		cout<<solve(x,c)<<"\n";
	}
	return 0;
}
