// http://codeforces.com/problemset/problem/131/D
// Very hacky..complicated
#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <cstdlib>
#include <cstdio>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

void bfs(vvi & al, vi & cycle){
	vector<bool> vis(al.size(),false);
	vi dis(al.size(),1000000007);
	queue<ll> q;
	for(ll i=0; i<cycle.size(); i++){
		dis[cycle[i]] = 0;
		vis[cycle[i]] = true;
//		cout<<cycle[i]<<" ";
		q.push(cycle[i]);
	}
//	cout<<endl;
	while(!q.empty()){
		ll x = q.front();
		vis[x] = true;
//		cout<<"current: "<<x<<"\n";
		for(ll i=0; i<al[x].size(); i++){
			if(!vis[al[x][i]]){
				dis[al[x][i]] = dis[x]+1;
				q.push(al[x][i]);
//				cout<<"pushing: "<<al[x][i]<<"\n";
			}
		}
//		getchar();
		q.pop();
	}
	for(ll i=0; i<dis.size(); i++){
		cout<<dis[i]<<" ";
	}
	cout<<endl;
}

bool printCycle(ll u, ll v, vi & par, vi & cycle){
	if(u==v){
		cycle.push_back(u);
//		cout<<u<<"\n";
		return true;
	}
	else{
		cycle.push_back(u);
//		cout<<u<<"\n";
//		getchar();
		return printCycle(par[u],v,par,cycle);
	}
}

bool found = false;

void dfsUtil(vvi & al, vector<bool> & vis, vi & par, ll src, vi & cycle){
	
	if(!found){
	vis[src] = true;
//	cout<<src<<"\n";
	for(ll i=0; i<al[src].size(); i++){
		if(!vis[al[src][i]]){
			par[al[src][i]] = src;
			dfsUtil(al,vis,par,al[src][i],cycle);
			if(found){
				return;
			}
		}else if(par[src] == al[src][i]){
		}else{
//			cout<<"PrintCycle called on "<<src<<" "<<al[src][i]<<"\n";
//			getchar();
			found = printCycle(src,al[src][i],par,cycle);
			return;
			//par[al[src][i]] = -1;
		}
	}
	}else{
		return;
	}
}

void dfs(vvi & al){
	vector<bool> vis(al.size(),false);
	vi par(al.size(),-1);
	vi cycle;
	par[0] = -1;
	dfsUtil(al,vis,par,0,cycle);
	/*
	for(int i=0; i<cycle.size(); i++){
		cout<<cycle[i]<<"\n";
	}
	*/
	bfs(al,cycle);
}


int main(int argc, char ** argv){
	//ios_base::sync_with_stdio(false);
	ll n;
	cin>>n;
	vvi al(n);
	for(int i=0; i<n; i++){
		ll a,b;
		cin>>a>>b;
		a--; b--;
		al[a].push_back(b);
		al[b].push_back(a);
	}
	dfs(al);
	return 0;
}
