#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;

double eps = 0.000001;

double f(ll R, ll H, double h){
	double L = sqrt(R*R + H*H);
	return (R*h*h*h + 3*L*h*h - 6*L*H*H);
}

double binSearch(ll R, ll H, double beg, double end){
//	cout<<"beg = "<<beg<<" f(beg) = "<<f(R,H,beg)<<" end = "<<end<<" f(end) = "<<f(R,H,end)<<"\n";
	if((end-beg)<eps) return (double)(beg+end)/(double)2.0;
	double mid = (double)(beg+end)/(double)2.0;
	
	if(f(R,H,mid) == 0){
		return mid;
	}else if(f(R,H,mid) < 0){
		return binSearch(R,H,mid,end);
	}else if(f(R,H,mid) > 0){
		return binSearch(R,H,beg,mid);
	}
}

double solve(ll R, ll H){
	if(f(R,H,H) <=  0) return H; 
	return binSearch(R,H,0,H);
}

int main(int argc, char ** argv){
	ll n;
	cin>>n;
	while(n--){
		ll R,H;
		cin>>R>>H;
		double h = solve(R,H);
		//cout<<"h = "<<h<<"\n";
		printf("%lf\n",h);
	}
	return 0;
}	
