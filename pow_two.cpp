// http://codeforces.com/contest/702/problem/B
/*
 * This code is awesome
 * Sunday 21 August 2016 02:35:39 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;

const ll inf = 1000000000+9;

ll searchub(vi & a, ll x, ll beg, ll end){
	if(end<beg) return -1;
	else if(beg == end){
		if(a[beg] == x) return beg;
		else return -1;
	}else if((beg+1)==end){
		if(a[end] == x) return end;
		if(a[beg] == x) return beg;
		else return -1;
	}
	ll mid = (ll)(beg+end)/2;
	if(a[mid] <= x){
		return searchub(a,x,mid,end);
	}else{
		return searchub(a,x,beg,mid-1);
	}
}

ll searchlb(vi & a, ll x, ll beg, ll end){
	if(end<beg) return -1;
	else if(beg == end){
		if(a[beg] == x) return beg;
		else return -1;
	}else if((beg+1)==end){
		if(a[beg] == x) return beg;
		if(a[end] == x) return end;
		else return -1;
	}
	ll mid = (ll)(beg+end)/2;
	if(a[mid] < x){
		return searchlb(a,x,mid+1,end);
	}else{
		return searchlb(a,x,beg,mid);
	}
}

int main(int argc, char ** argv){
	ll n;
	cin>>n;
	vi a(n,0);
	for(ll i=0; i<n; i++) cin>>a[i];
	sort(a.begin(),a.end());
	vector<long long> pow2(64,0);
	for(ll i=0; i<63; i++){
		pow2[i] = ((ll)(1))<<i;
	}
	
       	ll ans = 0;
	for(ll i=0; i<32; i++){
		for(ll j=0; j<n; j++){
			ll x1, x2;
			if((x1=searchlb(a,pow2[i]-a[j],j+1,a.size()-1))!= -1 && (x2=searchub(a,pow2[i]-a[j],j+1,a.size()-1))!= -1){
				ans += (x2-x1+1);	
			}
		}
	}
	cout<<ans<<"\n";
	
	return 0;
}
