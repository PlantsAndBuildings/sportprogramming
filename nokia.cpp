//https://www.codechef.com/problems/NOKIA
#include <cstdlib>
#include <iostream>
#include <vector>
#include <set>

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

int main(int argc, char ** argv){
	ll t,m,n;
	vector< set<ll> > dp(35);
	dp[1].insert(2);
	dp[2].insert(5);
	dp[3].insert(8);
	dp[3].insert(9);
	for(int i=4; i<=30; i++){
		for(set<ll>::iterator setIter = dp[i-1].begin(); setIter != dp[i-1].end(); setIter++){
			dp[i].insert((*setIter)+i+1);
		}
		for(int j=i-2; j>=1; j--){
			for(set<ll>::iterator iter1 = dp[j].begin(); iter1 != dp[j].end(); iter1++){
				for(set<ll>::iterator iter2 = dp[i-j-1].begin(); iter2 != dp[i-j-1].end(); iter2++){
					dp[i].insert((*iter1)+(*iter2)+i+1);
				}
			}
		}
	}
	/*
	for(int i=1; i<=30; i++){
		cout<<i<<" -> ";
		for(set<ll>::iterator setIter = dp[i].begin(); setIter != dp[i].end(); setIter++){
			cout<<(*setIter)<<" ";
		}
		cout<<"\n";
		

	}
	*/
	cin>>t;
	while(t--){
		cin>>n>>m;
		ll mindiff = 100005;
		set<ll>::iterator atleast = dp[n].begin();
		if(m < *atleast){
			cout<<"-1\n";
		}else{
			for(set<ll>::iterator setIter = dp[n].begin(); setIter != dp[n].end(); setIter++){
				if(abs(*setIter - m) < mindiff) mindiff = abs(*setIter - m);
			}
			cout<<mindiff<<"\n";
		}
	}
	return 0;
}
