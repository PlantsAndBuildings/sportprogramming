//http://codeforces.com/problemset/problem/437/C
#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

ll findMax(vi & val, vector<bool> & removed){
	ll i = 0;
	ll ret;
	while(removed[i]) i++;
	ret = i;
	for(; i<val.size(); i++){
		if(!removed[i] && val[i] > val[ret]){
			ret = i;
		}
	}
	return ret;
}

int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	ll n,m;
	cin>>n>>m;
	vi val(n,0);
	vi rm_val(n,0);
	for(int i=0; i<n; i++) cin>>val[i];
	vector<bool> removed(n,false);
	vvi al(n);
	for(int i = 0; i<m; i++){
		ll tmp1, tmp2;
		cin>>tmp1>>tmp2;
		tmp1--; tmp2--;
		al[tmp1].push_back(tmp2);
		al[tmp2].push_back(tmp1);
		rm_val[tmp1] += val[tmp2];
		rm_val[tmp2] += val[tmp1];
	}
	ll ans = 0;
	while(n--){
		ll rem = findMax(val,removed);
		removed[rem] = true;
		ans += rm_val[rem];
		for(int i=0; i<al[rem].size(); i++){
			if(!removed[al[rem][i]]){
				rm_val[al[rem][i]] -= val[rem];
			}
		}
	}
	cout<<ans<<"\n";
	return 0;
}
