//http://codeforces.com/contest/688/problem/C
#include <iostream>
#include <vector>
#include <set>

using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;


void dfs_util(vvi & al, vector<bool> & vis, vi & ws, ll src, ll state){
	vis[src] = true;
	ws[src] = state;
	for(ll i=0; i<al[src].size(); i++){
		if(!vis[al[src][i]]){
			if(state == 1){
				dfs_util(al,vis,ws,al[src][i],0);
			}else if(state == 0){
				dfs_util(al,vis,ws,al[src][i],1);
			}
		}
	}
}

void dfs(vvi & al, vector<bool> & vis, vi & ws){
	for(ll i=0; i<al.size(); i++){
		if(!vis[i]) dfs_util(al,vis,ws,i,0);
	}
}

int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	ll n,m;
	cin>>n>>m;
	vvi al(n);
	vii e(m,ii(-1,-1));
	vi ws(n,-1);
	for(ll i=0; i<m; i++){
		ll a,b;
		cin>>a>>b;
		a--; b--;
		al[a].push_back(b);
		al[b].push_back(a);
		e[i].first = a;
		e[i].second = b;
	}
	vector<bool> vis(n,false);
	dfs(al,vis,ws);
	/*
	for(ll i=0; i<n; i++){
		cout<<ws[i]<<" ";
	}
	cout<<"\n";
	*/
	vector< set<ll> > s(2);
	for(ll i=0; i<m; i++){
		if(ws[e[i].first] == ws[e[i].second]){
			cout<<"-1\n";
			return 0;
		}else{
			s[ws[e[i].first]].insert(e[i].first+1);
			s[ws[e[i].second]].insert(e[i].second+1);
		}
	}
	cout<<s[0].size()<<endl;
	for(set<ll>::iterator i=s[0].begin(); i != s[0].end(); i++) cout<<(*i)<<" ";
	cout<<endl;
	cout<<s[1].size()<<endl;
	for(set<ll>::iterator i=s[1].begin(); i != s[1].end(); i++) cout<<(*i)<<" ";
	cout<<endl;
	return 0;
}
