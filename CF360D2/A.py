#http://codeforces.com/contest/688/problem/A
n,d = map(int,raw_input().split())
cnt = 0
cnt_max = 0
for i in xrange(d):
    s = raw_input()
    if s == ("1"*n):
        cnt = 0
    else:
        cnt += 1
        if cnt > cnt_max:
            cnt_max = cnt
print cnt_max
