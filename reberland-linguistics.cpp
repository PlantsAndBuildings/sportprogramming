// http://codeforces.com/contest/666/problem/A
// Contest no 666 -- cool huh?
// Naive Recursion -- TLE as expected
#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <cstring>

using namespace std;

char s[10003];

set<vector<char> > suffs;

bool equal(vector<char> s1, vector<char> s2){
	if(s1.size() != s2.size()) return false;
	for(int i=0; i<s1.size(); i++){
		if(s1[i] != s2[i]){
			return false;
		}
	}
	return true;
}

void solve(char * s, int beg, int end, vector<char> suf){
	if(end-beg+1 < 5) return;
	//char * suf1 = (char*)malloc(sizeof(char)*3);
	//char * suf2 = (char *)malloc(sizeof(char)*4);
	vector<char> suf1(3);
	vector<char> suf2(4);
	suf1[0] = s[end-1];
	suf1[1] = s[end];
	suf1[2] = '\0';
	suf2[0] = s[end-2];
	suf2[1] = s[end-1];
	suf2[2] = s[end];
	suf2[3] = '\0';
	if(!equal(suf,suf1)){
		if(end-beg+1-2 >= 5){
			suffs.insert(suf1);
			solve(s,beg,end-2,suf1);
		}
	}
	if(!equal(suf,suf2)){
		if(end-beg+1-3 >= 5){
			suffs.insert(suf2);
			solve(s,beg,end-3,suf2);
		}

	}
}

int main(int argc, char ** argv){
	cin>>s;
	// cout<<s;
	// cout<<strlen(s)<<"\n";
	vector<char> suf(1);
	solve(s,0,strlen(s)-1,suf);
	cout<<suffs.size()<<"\n";
	for(set<vector<char> >::iterator setIter = suffs.begin(); setIter != suffs.end(); setIter++){
		for(int i=0; i<setIter->size()-1; i++){
			cout<<(*setIter)[i];
		}
		cout<<"\n";
	}
	return 0;
}
