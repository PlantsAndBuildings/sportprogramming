#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;

ll min(ll a, ll b){
	return a<b?a:b;
}

ll solve(ll curr_len, vi & snake, ll beg, ll end){
	for(ll i = beg; i<=end; i++){
		if(curr_len > snake[i]){
			// gobble
			curr_len += snake[i];
		}else{
			// cout<<"beg = "<<beg<<" end = "<<end<<" i = "<<i<<"\n";
			// either introduce new snake or remove all other snakes
			ll l =0;
			while((((1<<l)*(curr_len-1))+1) <= snake[i] ) l++;
			return min(l+solve((1<<l)*(curr_len-1)+1+snake[i],snake,i+1,end),(snake.size()-i));
		}
	}
	return 0;
}

int main(int argc, char ** argv){
	ll t;
	cin>>t;
	while(t--){
		ll x, n;
		cin>>x>>n;
		vi s(n,0);
		for(int i=0; i<n; i++) cin>>s[i];
		sort(s.begin(), s.end());
		//for(int i=0; i<n; i++) cout<<s[i]<<" ";
		//cout<<"\n";
		cout<<solve(x,s,0,n-1)<<"\n";
	}
	return 0;
}
