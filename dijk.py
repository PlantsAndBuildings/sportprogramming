#http://codeforces.com/problemset/problem/20/C
class Graph:

	V = 0
	al = []
	cyclic = False
	weighted = False
	recStack = []	
	topoSort = []
        sp_par = []
	inf = 1000000009

	def __init__(self,V,weighted=False):
		self.V = V
		self.al = [[] for i in range(V)]
		self.isCyclic = False
		self.recStack = []
		self.topoSort = []
		self.sp_par = [-1 for i in range(self.V)]
                self.weighted = weighted
		self.inf = 1000000000+9
	
	def __str__(self):
		return "Number of Vertices: "+str(self.V)+"\n"\
		+"Adjacency List: "+str(self.al)+"\n"\
		+"isCyclic: "+str(self.cyclic)+"\n"\
		+"Topological Sorting (only if isCyclic is False): "+str(self.topoSort)

	def addEdge(self, u, v, directed=False, weight=None):
		if not self.weighted:
			if not directed:
				self.al[u].append(v)
				self.al[v].append(u)
			else:
				self.al[u].append(v)
		else:
			if not directed:
				self.al[u].append((v,weight))
				self.al[v].append((u,weight))
			else:
				self.al[u].append((v,weight))

	def DFS(self):
		visited = [False for i in range(self.V)]
		for vertex in self.al:
			if not visited[self.al.index(vertex)]:
				self.DFSUtil(visited,self.al.index(vertex))
	
	def DFSUtil(self,visited,source):
		visited[source] = True
		print source
		self.topoSort.append(source)
		self.recStack.append(source)
		for vertex in self.al[source]:
			if not self.weighted:
				if vertex in self.recStack:
					self.cyclic = True
				if not visited[vertex]:
					self.DFSUtil(visited,vertex)
			else:
				if vertex[0] in self.recStack:
					self.cyclic = True
				if not visited[vertex[0]]:
					self.DFSUtil(visited,vertex[0])
		self.recStack.pop()
	
	def Dijkstra(self,source):
		if self.weighted:
			sptSetPrime = {v:self.inf for v in range(self.V)}
			sptSetPrime.pop(source)
			sptSet = {source:0}
			curr_min = source
			while len(sptSetPrime) != 0:
				#print sptSet
				#print sptSetPrime	# Uncomment these lines to see the sptSet and sptSetPrime as Dijkstra proceeds
				for vertex in self.al[curr_min]:
					if vertex[0] not in sptSet.keys():
						sptSetPrime[vertex[0]] = min(sptSetPrime[vertex[0]],sptSet[curr_min]+vertex[1])
                                                if sptSetPrime[vertex[0]] == sptSet[curr_min]+vertex[1]:
                                                        self.sp_par[vertex[0]] = curr_min    
				min_dis = self.inf
				for vertex,distance in sptSetPrime.items():
					if distance <= min_dis:
						curr_min = vertex
						min_dis = distance
				curr_min_val = sptSetPrime.pop(curr_min)
				sptSet[curr_min] = curr_min_val
			return sptSet,self.sp_par
		else:
			return None
 				

n,m = map(int,raw_input().split())
g = Graph(n,weighted=True)
for i in range(m):
        a,b,w = map(int,raw_input().split())
        g.addEdge(a-1,b-1,weight=w)
spt_set, spt_tree = g.Dijkstra(0)
if spt_set[n-1] == 1000000009:
        print "-1"
else:
        lst = []
        curr = n-1
        while spt_tree[curr] != -1:
                lst.append(curr+1)
                curr = spt_tree[curr]
        lst.append(1)
        s = ""
        for elem in lst[::-1]:
                s += str(elem)+" "
        print s
