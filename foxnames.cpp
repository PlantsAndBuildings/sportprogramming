//http://codeforces.com/problemset/problem/510/C
#include <iostream>
#include <vector>
#include <string>
#include <stack>

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

class Graph{
	private:
		ll V;
		vvi al;
		bool isDigraph;
		void DFSUtil(vvi &, vector<bool>&, ll);
		vi toposort;
		vi rec_stack;
		ll st_top;
	public:
		bool isCyclic;
		Graph(ll);
		Graph(ll,bool);
		void addEdge(ll,ll);
		void DFS();
		void DFS(ll);
		void printGraph();
		void printTopoSort();
};

Graph::Graph(ll V){
	this->V = V;
	al.resize(V);
	rec_stack.resize(V);
	this->isDigraph = false;
	this->isCyclic = false;
	this->st_top = 0;
}

Graph::Graph(ll V, bool isDigraph){
	this->V = V;
	al.resize(V);
	rec_stack.resize(V);
	this->isDigraph = isDigraph;
	this->isCyclic = false;
	this->st_top = 0;
}

void Graph::addEdge(ll u, ll v){
	if(isDigraph){
		al[u].push_back(v);
	}else{
		al[u].push_back(v);
		al[v].push_back(u);
	}
}

void Graph::DFS(){
	vector<bool> visited(V,false);
	for(ll i=0; i<V; i++){
		if(!visited[i]){
			DFSUtil(al,visited,i);
		}
	}
}

// Use this function when it is given that graph is connected and we have to start DFS from a particular node--
// For Example, this may be used in case of a tree (DFS from the root)
void Graph::DFS(ll source){
	vector<bool> visited(V,false);
	DFSUtil(al,visited,source);
}

void Graph::DFSUtil(vvi & al, vector<bool> & visited, ll source){
	visited[source] = true;
	rec_stack[st_top] = source;
	st_top++;
	//cout<<source<<"\n";		// Uncomment this line to check the DFS order.
	for(vi::iterator viIter = al[source].begin(); viIter != al[source].end(); viIter++){
		for(ll i=0; i<st_top; i++){
			if(rec_stack[i] == (*viIter)){
				isCyclic = true;
			}
		}
		if(!visited[*viIter]){
			DFSUtil(al,visited,*viIter);
		}
	}
	st_top--;
	toposort.push_back(source);

}

void Graph::printGraph(){
	ll v_no = 0;
	for(vvi::iterator vviIter = al.begin(); vviIter != al.end(); vviIter++, v_no++){
		cout<<v_no<<"->";
		for(vi::iterator viIter = (*vviIter).begin(); viIter != (*vviIter).end(); viIter++){
			cout<<(*viIter)<<" ";
		}
		cout<<endl;
	}
}

void Graph::printTopoSort(){
	for(vi::iterator viIter = toposort.begin(); viIter != toposort.end(); viIter++){
		cout<<((char)(*viIter + 'a'));
	}
	cout<<endl;
}

bool buildGraph(Graph & g, vector<string> & name, ll i, ll j){
	ll k = 0;
	while(name[i][k] == name[j][k] && k != name[i].length() && k != name[j].length()) k++;
	if(k == name[i].length() && k == name[j].length()){
		return true;
	}else if(k == name[i].length() && k != name[j].length()){
		return true;
	}else if(k != name[i].length() && k == name[j].length()){
		return false;
	}else{
		g.addEdge((ll)(name[j][k] - 'a'),(ll)(name[i][k] - 'a'));
		return true;
	}
}

int main(int argc, char ** argv){
	ll n;
	cin>>n;
	vector<string> name(n);
	for(ll i=0; i<n; i++){
		cin>>name[i];
	}
	Graph g(26,true);
	for(ll i=0; i<n; i++){
		for(ll j=i+1; j<n; j++){
			if(buildGraph(g,name,i,j));
			else{
				cout<<"Impossible\n";
				return 0;
			}
		}
	}
	//g.printGraph();
	g.DFS();
	if(g.isCyclic){
		cout<<"Impossible\n";
	}else{
		g.printTopoSort();
	}
	return 0;
}
