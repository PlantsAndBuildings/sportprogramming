#include <iostream>
#include <vector>

using namespace std;

typedef long long int ll;

bool repfree(ll num){
	vector<bool> present(10,false);
	while(num != 0){
		if(num%10 == 0){
			return false;
		}
		if(present[num%10]){
			return false;
		}else{
			present[num%10] = true;
			num = num/10;
		}
	}
	return true;
}

int main(int argc, char ** argv){
	/*
	cout<<repfree(1000)<<"\n";
	cout<<repfree(1234)<<"\n";
	cout<<repfree(1024)<<"\n";
	*/
	ll n;
	cin>>n;
	if(n>=987654321){
		cout<<"0\n";
		return 0;
	}
	for(int i=n+1; ; i++){
//		cout<<i<<"\n";
		if(repfree(i)){
			cout<<i<<"\n";
			break;
		}
	}
	
	return 0;
}
