#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;

int main(int argc, char ** argv){
	ll n,k;
	cin>>n>>k;
	if(k > (ll)((n-1)/2)){
		cout<<"-1\n";
		return 0;
	}else{
		cout<<(n*k)<<"\n";
		for(ll i=0; i<n; i++){
			for(ll j=1; j<=k; j++){
				cout<<(i+1)<<" "<<(((i+j)%n)+1)<<"\n";
			}
		}
	}
	return 0;
}
