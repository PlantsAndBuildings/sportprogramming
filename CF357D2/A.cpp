#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <string>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ll n;
	cin>>n;
	string tmp;
	ll bef;
	ll aft;
	bool flag = false;
	for(int i=0; i<n; i++){
		cin>>tmp>>bef>>aft;
		if(bef>=2400 && aft>bef){
			//cout<<"YES\n";
			//return 0;
			flag = true;
		}
		
	}
	if(flag) cout<<"YES\n";
	else cout<<"NO\n";
	return 0;
}
