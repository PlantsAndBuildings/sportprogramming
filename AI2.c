#include <stdio.h>
#include <stdlib.h>

typedef struct state{
	int dat[9];
	int posZero;
	int code;
	struct state * par;
	struct state * nextState[4];
}state;

enum action{
	UP, DOWN, LEFT, RIGHT
};

enum boolean{
	false, true
};

typedef struct queue{
	int front;
	int back;
	int maxSz;
	state * elements[362880+5];
}queue;

int factorial[10] = {1,1,2,6,24,120,720,5040,40320,362880};


queue * getQueue(){
	queue * q = (queue*)malloc(sizeof(queue));
	q->front = 0;
	q->back = 0;
	q->maxSz = 362880+5;
	return q;
}

state * front(queue * q){
	if(q->front == q->back){
		printf("Queue is empty!\n");
		return NULL;
	}
	return q->elements[q->front];
}

void dequeue(queue * q){
	if(q->front == q->back) return;
	q->front++;
	q->front %= q->maxSz;
	return;
}

void enqueue(queue * q, state * s){
	if(((q->back)+1)%(q->maxSz) == q->front){
		printf("Queue is full. Enqueue failed.\n");
		return;
	}
	q->elements[q->back] = s;
	q->back++;
	q->back %= q->maxSz;
	return;
}

int empty(queue * q){
	if(q->front == q->back) return true;
	else return false;
}



int getCode(int * arr){
	int i,j;
	int tmp[9];
	for(i=0; i<9; i++) tmp[i] = 0;
	for(i=0; i<9; i++){
		for(j=i+1; j<9; j++){
			if(arr[i]>arr[j]) tmp[i]++;
		}
	}
	int code = 0;
	for(i=0; i<9; i++) code += tmp[i]*factorial[8-i];
	return code;
}

state * getState(int * dat){
	int i;
	state * ret = (state *)malloc(sizeof(state));
	for(i=0; i<9; i++){
		ret->dat[i] = dat[i];
		if(dat[i] == 0) ret->posZero = i;
	}
	ret->code = getCode(ret->dat);
	ret->par = NULL;
	ret->nextState[UP] = NULL;
	ret->nextState[DOWN] = NULL;
	ret->nextState[LEFT] = NULL;
	ret->nextState[RIGHT] = NULL;
	return ret;
}

void printState(state * s){
	if(s == NULL) return;
	int i;
	for(i=0; i<9; i++) printf("%d ",s->dat[i]);
	printf("\n");
	return;
}

state * getChildState(state * curr, enum action a){
	switch(a){
		case UP:
			if(curr->posZero == 0 || curr->posZero == 1 || curr->posZero == 2){
				return NULL;
			}else{
				state * child = getState(curr->dat);
				int tmp = child->dat[child->posZero];
				child->dat[child->posZero] = child->dat[child->posZero-3];
				child->dat[child->posZero-3] = tmp;
				child->posZero = child->posZero-3;
				child->par = curr;
				child->code = getCode(child->dat);
				curr->nextState[UP] = child;
				return child;
			}
		
		case DOWN:
			if(curr->posZero == 6 || curr->posZero == 7 || curr->posZero == 8){
				return NULL;
			}else{
				state * child = getState(curr->dat);
				int tmp = child->dat[child->posZero];
				child->dat[child->posZero] = child->dat[curr->posZero+3];
				child->dat[child->posZero+3] = tmp;
				child->posZero = child->posZero+3;
				child->par = curr;
				child->code = getCode(child->dat);
				curr->nextState[DOWN] = child;
				return child;
			}

		case LEFT:
			if(curr->posZero == 0 || curr->posZero == 3 || curr->posZero == 6){
				return NULL;
			}else{
				state * child = getState(curr->dat);
				int tmp = child->dat[child->posZero];
				child->dat[child->posZero] = child->dat[curr->posZero-1];
				child->dat[child->posZero-1] = tmp;
				child->posZero = child->posZero-1;
				child->par = curr;
				child->code = getCode(child->dat);
				curr->nextState[LEFT] = child;
				return child;
			}

		case RIGHT:
			if(curr->posZero == 2 || curr->posZero == 5 || curr->posZero == 8){
				return NULL;
			}else{
				state * child = getState(curr->dat);
				int tmp = child->dat[child->posZero];
				child->dat[child->posZero] = child->dat[curr->posZero+1];
				child->dat[child->posZero+1] = tmp;
				child->posZero = child->posZero+1;
				child->par = curr;
				child->code = getCode(child->dat);
				curr->nextState[RIGHT] = child;
				return child;
			}
	}
	
}

enum boolean equal(state * s1, state * s2){
	int i;
	for(i=0; i<9; i++) if(s1->dat[i] != s2->dat[i]) return false;
	return true;
}

void breadthFirstSearch(state * init, state * final){
	int i;
	queue * q = getQueue();
	enqueue(q,init);
	int visited[362885];
	for(i=0; i<362885; i++) visited[i] = false;
	while(1){
		state * curr = front(q);
		
		printf("Current State: ");
		printState(curr);
		//printf("Curr code = %d\n",curr->code);
		if(curr == NULL){
			printf("State not found!\n");
			return;
		}else if(equal(curr,final)){
			visited[curr->code] = true;
			printf("State found!\n");
			return;
		}else{
			if(visited[curr->code] == true){
				printf("already visited\n");
				goto END;
			}
			visited[curr->code] = true;
			state * chUp = getChildState(curr,UP);
			state * chDown = getChildState(curr,DOWN);
			state * chLeft = getChildState(curr,LEFT);
			state * chRight = getChildState(curr,RIGHT);
			if(chUp != NULL && visited[chUp->code] == false){
				enqueue(q,chUp);
				printf("Enqueing: ");
				printState(chUp);
				printf("%d\n",chUp->code);
			}
			if(chDown != NULL && visited[chDown->code] == false){
				enqueue(q,chDown);
				printf("Enqueing: ");
				printState(chDown);
				printf("%d\n",chDown->code);
			}
			if(chLeft != NULL && visited[chLeft->code] == false){
				enqueue(q,chLeft);
				printf("Enqueing: ");
				printState(chLeft);
				printf("%d\n",chLeft->code);
			}
			if(chRight != NULL && visited[chRight->code] == false){
				enqueue(q,chRight);
				printf("Enqueing: ");
				printState(chRight);
				printf("%d\n",chRight->code);
			}
		}
END:		free(curr);
		dequeue(q);
		//getchar();
	}
}

void printQueue(queue * q){
	if(empty(q)){
		printf("Queue is empty.\n");
		return;
	}
	int i;
	printf("Front = %d\tBack = %d\n",q->front,q->back);
	for(i = q->front; i != q->back; i++,i%=(q->maxSz)) printState(q->elements[i]);
	return;
}

int main(int argc, char ** argv){
	/* Testing the queue */
	/*
	queue * q = getQueue();
	int tmp;
	int tmp2[9] = {0,0,0,0,0,0,0,0,0};
	while(1){
		tmp2[0]++;
		state * s;
		scanf("%d",&tmp);
		if(tmp == 0){	// enqueue
			s = getState(tmp2);
			printf("Enqueuing element ");
			printState(s);
			enqueue(q,s);
			printf("Queue\n");
			printQueue(q);
		}else if(tmp == 1){	// dequeue
			s = front(q);
			printf("Removing element ");
			printState(s);
			dequeue(q);
			printf("Queue:\n");
			printQueue(q);
		}
	}
	*/
	/* Testing the Search  */
	int init_config[9] = {1,2,3,4,5,6,8,7,0};
	int final_config[9] = {1,2,3,4,5,6,7,8,0};
	state * init = getState(init_config);
	state * final = getState(final_config);
	printState(init);
	printState(final);
	breadthFirstSearch(init,final);
	return 0;
}
