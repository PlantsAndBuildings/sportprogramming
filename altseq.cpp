// http://www.spoj.com/problems/ALTSEQ/
// Easy DP.. Modification of LIS. Took way too many submissions to get right.
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;

const ll inf = 1000000000+9;

int main(int argc, char ** argv){
    //ios_base::sync_with_stdio(false);
    ll n;
    //cin>>n;
    scanf("%lld",&n);
    vi a(n,0), dp(n,-inf);
    for(ll i=0; i<n; i++) /*cin>>a[i];*/ scanf("%lld",&a[i]);
    dp[0] = 1;
    for(ll i=1; i<n; i++){
        for(ll k=0; k<i; k++){
            if(a[i]*a[k] < 0 && abs(a[k])<abs(a[i])){
                dp[i] = max(dp[i],dp[k]+1);
            }
        }
        if(dp[i] == -inf) dp[i] = 1;
    }
    ll maxim = -inf;
    for(ll i=0; i<n; i++){
        if(dp[i] > maxim) maxim = dp[i];
    }
    printf("%lld\n",maxim);
    //cout<<maxim<<"\n";
    return 0;
}
