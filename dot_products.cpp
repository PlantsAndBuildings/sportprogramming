#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;

int main(int argc, char ** argv){
	ll n;	
	cin>>n;
	vi u(n,0), v(n,0);
	for(int i=0; i<n; i++){
		cin>>u[i];
	}

	for(int i=0; i<n; i++){
		cin>>v[i];
	}

	sort(u.begin(), u.end());
	sort(v.begin(), v.end());

	ll sum =0;
	for(int i=0; i<n; i++){
		sum += u[i]*v[n-i-1];
	}
	cout<<sum<<"\n";
	return 0;
}
