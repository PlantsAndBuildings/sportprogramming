//https://www.codechef.com/problems/NUMFACT
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ll t,n;
	//fstream fin,fout;
	//fin.open("in",ios::in);
	//fout.open("out",ios::out);
	//fin>>t;
	scanf("%lld",&t);
	vector<bool> isPrime(1000005,true);
	for(int i=2;i<1000002;i++){
		if(isPrime[i]){
			int j = i*2;
			while(j < 1000004){
				isPrime[j] = false;
				j += i;
			}
		}
	}
	vi primes;
	for(int i=2;i<=1000000;i++){
		if(isPrime[i]) primes.push_back(i);
	}
	/*
	for(int i=0;i<primes.size();i++){
		fout<<primes[i]<<" ";
	}
	fout<<"\n";
	fout<<primes.size()<<"\n";
	*/
	ll num_primes = primes.size();
	while(t--){
		//fin>>n;
		scanf("%lld",&n);
		vi arr(n,0);
		vi cnt(num_primes,0);
		for(int i=0;i<n;i++){
			//fin>>arr[i];
			scanf("%lld",&arr[i]);
			for(int j=0;j<num_primes && arr[i] >= primes[j];j++){
				while(arr[i]%primes[j]==0){
			//		cout<<"poops arr[i] = "<<arr[i]<<"\n";
					arr[i]/=primes[j];
					cnt[j]++;
				}
			//	cout<<"poops arr[i] = "<<arr[i]<<"\n";
			}
		}
		ll num_factors = 1;
		for(int i=0; i < num_primes; i++){
			num_factors *= cnt[i]+1; 
		}
		//fout<<num_factors<<"\n";
		printf("%lld\n",num_factors);
	}
	return 0;
}
