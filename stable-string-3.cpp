#include <iostream>
#include <vector>
#include <stack>
#include <string>

using std::cout;
using std::cin;
using std::vector;
using std::stack;
using std::string;

int main(int argc, char ** argv){
	int ctr = 1;
	while(1){
		stack<char> st;
		string s;
		cin>>s;
		if(s[0] == '-') break;
		int ans = 0;
		for(int i=0; i<s.length(); i++){
			if(s[i] == '{'){
				if(st.empty()){
					st.push('{');
				}else{
					st.push('{');
				}
			}else if(s[i] == '}'){
				if(st.empty()){
					ans++;
					st.push('{');
				}else{
					st.pop();
				}
			}
		}
		ans += st.size()/2;
		cout<<ctr<<". "<<ans<<"\n";
		ctr++;
	}

	return 0;
}
