//www.codechef.com/problems/FNCS
//This code doesnt pass anything.. but i have implemented Sqrt Decomposition here
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <set>


#define fs first
#define sc second

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

bool cmpfn(const pair<ii,ll> & p1, const pair<ii,ll> & p2){
	if(p1.fs.fs < p2.fs.fs) return true;
	else if(p1.fs.fs == p2.fs.fs){
		if(p1.fs.sc <= p2.fs.sc) return true;
	}
	return false;
}

int main(int argc, char ** argv){
	//ios_base::sync_with_stdio(false);
	fstream fin,fout;
	fin.open("in",ios::in);
	fout.open("out",ios::out);
	ll n;
	//cin>>n;
	fin>>n;
	vi a(n,0);
	vi bkt(320,0);
	vector< pair<ii,ll> > f(n);
	vector< pair<ii,ll> > g(n);
	for(int i=0;i<n;i++){
		//cin>>a[i];
		fin>>a[i];
	}
	for(int i=0;i<n;i++){
		bkt[(int)(i/320)] += a[i];
	}
	for(int i=0;i<n;i++){
		ll l,r;
		fin>>f[i].fs.fs>>f[i].fs.sc;
		g[i].fs.fs = f[i].fs.fs;
		g[i].fs.sc = f[i].fs.sc;
	}
	sort(f.begin(),f.end(),cmpfn);
	//check the sorting here
	set<ll> curr;
	vector< set<ll> > fnl(n);
	vector< set<ll> > ends(n);
	for(int i=0;i<n;i++){
		curr.insert(i);
		for(set<ll>::iterator it = curr.begin(); it!=curr.end(); it++){
			fnl[i].insert(*it);
		}
		ends[f[i].fs.sc].insert(i);
		for(set<ll>::iterator it = ends[i].begin(); it != ends[i].end(); it++){
			curr.erase(*it);
		}
	}
	//check whether the correct functions are active at the correct points.
	ll q;
	//cin>>q;
	fin>>q;
	for(int i=0;i<q;i++){
		ll qt;
		//cin>>qt;
		fin>>qt;
		if(qt==1){
			// replace xth element by y
			ll x,y;
			//cin>>x>>y;
			fin>>x>>y;
			x--;
			ll tmp = a[x];
			a[x] = y;
			bkt[(int)(x/320)] -= tmp;
			bkt[(int)(x/320)] += y;

		}else if(qt==2){
			// return sum from mth elemt to nth elemt
			ll m,n2;
			//cin>>m>>n2;
			fin>>m>>n2;
			ll ret = 0;
			ll x = (ll)(m/320);
			ll y = (ll)(n2/320);
			for(int j=x+1;j<=y-1;j++) ret += bkt[j];
			for(int j=m;j<x*320;j++) ret += a[j];
			for(int j=y*320;j<n2;j++) ret += a[j];
			//cout<<ret<<"\n";
			fout<<ret<<"\n";
		}
	}
	return 0;
}
