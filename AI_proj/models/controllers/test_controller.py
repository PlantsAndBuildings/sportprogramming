from math import *
from utils import linalg2_util as linalg
from utils import math_util
import numpy as np
import skfuzzy as fuzz

REFRESH_RATE = 20.0


K3_SENSOR_POSES = [[ -0.038,  0.048,  128 ], [  0.019,  0.064,  75  ],[  0.050,  0.050,  42  ],[  0.070,  0.017,  13  ],[  0.070, -0.017, -13  ],[  0.050, -0.050, -42  ],[  0.019, -0.064, -75  ],[ -0.038, -0.048, -128 ],[ -0.048,  0.000,  180 ]]

class TestController:
  def __init__(self,supervisor):
    self.supervisor = supervisor
    self.prev_time = 0.0
    self.previous_angle = 0
    self.current_angle = 0
    self.NUM_RULES = 8
    self.THRESHOLD = 20.0
    self.test_heading_vector = [1.0 , 1.0]
    self.PI = 3.1415926
    self.alpha = [0 for i in range(8)]
    self.right_dis = 0
    self.left_dis = 0
    self.front_dis = 0
    self.sensor_distances = []
    self.P = 0
    self.C1 = 0.08
    self.C2 = 0.36
    self.speeds = [0 for i in range(self.NUM_RULES)]
    self.speeds[0] = 0
    self.speeds[1] = self.C1 * 0.3148 #self.supervisor.v_max()
    self.speeds[2] = self.C2 * 0.3148 #self.supervisor.v_max()
    self.speeds[3] = self.C2 * 0.3148 #self.supervisor.v_max()
    self.speeds[4] = self.C1 * 0.3148 #self.supervisor.v_max()
    self.speeds[5] = self.C1 * 0.3148 #self.supervisor.v_max()
    self.speeds[6] = self.C2 * 0.3148 #self.supervisor.v_max()
    self.speeds[7] = 0.3148 #self.supervisor.v_max()
    self.Q = []
    self.phi = []
    for i in range(self.NUM_RULES):
        self.Q.append([0 for j in range(6)])
    # Rule 1
    self.phi.append([((self.PI/3.0)+((2.0*self.PI)/15.0)*j)*(180.0/self.PI) for j in range(6)])
    # Rule 2
    self.phi.append([-((self.PI/3.0)+((2.0*self.PI)/15.0)*j)*(180.0/self.PI) for j in range(6)])
    # Rule 3
    self.phi.append([-((self.PI/3.0)-((2.0*self.PI)/15.0)*j)*(180.0/self.PI) for j in range(6)])
    # Rule 4
    self.phi.append([-((2.0*self.PI)/15.0)*(180.0/self.PI)*j for j in range(6)])
    # Rule 5
    self.phi.append([((self.PI/3.0)+((2.0*self.PI)/15.0)*j)*(180.0/self.PI) for j in range(6)])
    # Rule 6
    self.phi.append([(self.PI/3.0+((2.0*self.PI)/15.0)*j)*(180.0/self.PI) for j in range(6)])
    # Rule 7
    self.phi.append([((2.0*self.PI)/15.0)*j*(180.0/self.PI) for j in range(6)])
    # Rule 8
    # To goal
    #self.phi.append(self.get_angle_to_goal())
    self.phi.append(0)
    self.previous_left_dis = 0
    self.previous_right_dis = 0
    self.previous_front_dis = 0
    self.current_pose = 0
    self.previous_pose = 0
    self.previous_dis_from_goal = 0
    self.current_dis_from_goal = 0

    self.learning_rate = 0.5
    self.discount_factor = 0.1  
    self.Q_curr = 0
    self.Q_prev = 0
    self.d_Q_curr = 0

  def update_heading(self):
    self.test_heading_vector = self.calculate_test_heading_vector()

  def calculate_test_heading_vector(self):
    return [1.0 , 1.0]
  
  def get_angle_to_goal(self): 
    robot_pose = self.supervisor.estimated_pose()
    goal = self.supervisor.goal()
    dis = ((goal[0] - robot_pose.x)**2 + (goal[1]-robot_pose.y)**2)**0.5
    if goal[1] >=0:
        return (180.0/self.PI)*acos((1.0/dis)*((goal[0]-robot_pose.x)*cos(robot_pose.theta) + (goal[1] - robot_pose.y)*sin(robot_pose.theta)))
    else:
        return -(180.0/self.PI)*acos((1.0/dis)*((goal[0]-robot_pose.x)*cos(robot_pose.theta) + (goal[1] - robot_pose.y)*sin(robot_pose.theta)))

  def calculate_dis_from_goal(self):
    robot_pose = self.supervisor.estimated_pose()
    goal = self.supervisor.goal()
    return ((goal[0] - robot_pose.x)**2 + (goal[1]-robot_pose.y)**2)**0.5

  
  def calculate_steering_angle(self):
    self.phi[self.NUM_RULES-1] = self.current_angle - self.get_angle_to_goal()
    phi_i_m = [0 for i in range(self.NUM_RULES)]
    for i in range(self.NUM_RULES - 1):
        j = np.argmax(self.Q[i])
        phi_i_m[i] = self.phi[i][j]
    phi_i_m[self.NUM_RULES - 1] = self.get_angle_to_goal()
    phi_i_m[0] = (2*self.P - 1)*phi_i_m[0]
    phi_i_m[5] = (2*self.P - 1)*phi_i_m[5]

    return np.dot(self.alpha,phi_i_m)/sum(self.alpha)


  
  def calculate_truth_vals(self):
    self.sensor_distances = self.supervisor.proximity_sensor_distances()
    self.front_dis = (self.sensor_distances[3]*cos(13*self.PI/180.0)+self.sensor_distances[4]*cos(13*self.PI/180.0)+self.sensor_distances[2]*cos(42.0*self.PI/180.0)+self.sensor_distances[5]*cos(42*self.PI/180.0))/((cos(13*self.PI/180.0)+ cos(42*self.PI/180.0))*2.0)
    self.left_dis = (self.sensor_distances[1]*sin(75.0*self.PI/180.0) + self.sensor_distances[2]*sin(42*self.PI/180.0))/(sin(75.0*self.PI/180.0)+sin(42*self.PI/180.0))
    self.right_dis = (self.sensor_distances[5]*sin(75.0*self.PI/180.0) + self.sensor_distances[6]*sin(42*self.PI/180.0))/(sin(75.0*self.PI/180.0)+sin(42*self.PI/180.0))
    
    if self.left_dis > self.right_dis:
        self.P = 1
    else:
        self.P = 0


    if self.right_dis >= 0.15:
        r_N = 0.0
        r_F = 1.0
    elif self.right_dis <= 0.07:
        r_N = 1.0
        r_F = 0.0
    else:
        r_N = ((-self.right_dis+0.15)/0.08)
        r_F = 1 - r_N  
    
    if self.front_dis >= 0.15:
        f_N = 0.0
        f_F = 1.0
    elif self.front_dis <= 0.07:
        f_N = 1.0
        f_F = 0.0
    else:
        f_N = ((-self.front_dis+0.15)/0.08)
        f_F = 1 - f_N

    if self.left_dis >= 0.15:
        l_N = 0.0
        l_F = 1.0
    elif self.left_dis <= 0.07:
        l_N = 1.0
        l_F = 0.0
    else:
        l_N = ((-self.left_dis+0.15)/0.08)
        l_F = 1 - l_N
   
    self.alpha[0] = (l_N*f_N*r_N)
    self.alpha[1] = (l_N*f_N*r_F)
    self.alpha[2] = (l_N*f_F*r_N)
    self.alpha[3] = (l_N*f_F*r_F)
    self.alpha[4] = (l_F*f_N*r_N)
    self.alpha[5] = (l_F*f_N*r_F)
    self.alpha[6] = (l_F*f_F*r_N)
    self.alpha[7] = (l_F*f_F*r_F)
    
    
  def get_reinforcement(self):
    if self.front_dis < self.previous_front_dis and self.left_dis > self.previous_left_dis and self.right_dis < self.previous_right_dis and self.current_dis_from_goal < self.previous_dis_from_goal:
        return 30
    elif max(self.left_dis, self.right_dis, self.front_dis) < self.THRESHOLD and self.current_dis_from_goal < self.previous_dis_from_goal:
        return 20
    elif  self.front_dis < self.previous_front_dis and self.left_dis > self.previous_left_dis and self.right_dis < self.previous_right_dis:
        return 10
    elif  max(self.left_dis, self.right_dis, self.front_dis) < self.THRESHOLD:
        return 5
    else:
        return -2

    

  def execute(self):
    current_time = self.supervisor.time()
    dt = current_time - self.prev_time
    
    
    self.phi[self.NUM_RULES - 1] = self.get_angle_to_goal()
    print("Angle to goal: "+str(self.phi[self.NUM_RULES-1]))
    self.calculate_truth_vals()
    print("truth values: "+str(self.alpha))
    print("SL = "+str(self.left_dis))
    print("SF = "+str(self.front_dis))
    print("SR = "+str(self.right_dis))
    self.current_pose = self.supervisor.estimated_pose()
    print("Current pose: "+ str(self.current_pose))
    self.current_angle = self.current_pose.vunpack()[1]*(180.0/self.PI)#self.calculate_steering_angle()
    print("Current angle: "+ str(self.current_angle))
    self.current_dis_from_goal = self.calculate_dis_from_goal()
    print("Current dis from goal : "+ str(self.current_dis_from_goal))
    d_angle = (self.get_angle_to_goal())# - self.current_angle)
    self.current_steering_angle = self.calculate_steering_angle()
    print("Current steering angle : "+str(self.current_steering_angle))
    v = np.dot(self.speeds,self.alpha)/sum(self.alpha)
    omega = (self.supervisor.omega_max()*((self.current_steering_angle/180.0)**1))*(self.supervisor.v_max()/v)
    #v = self.supervisor.v_max()
    #omega = 1
    self.supervisor.set_outputs(v,omega)
    q_i_m = [0 for i in range(self.NUM_RULES)]
    for i in range(self.NUM_RULES):
        j = np.argmax(self.Q[i])
        q_i_m[i] = self.Q[i][j]
    self.Q_curr = np.dot(self.alpha, q_i_m)
    r_signal = self.get_reinforcement()
    self.d_Q_curr = self.learning_rate*(r_signal + self.discount_factor*self.Q_curr - self.Q_prev)
    for i in range(self.NUM_RULES):
        j = np.argmax(self.Q[i])
        self.Q[i][j] += self.alpha[i] * self.d_Q_curr
    self.Q_prev = self.Q_curr
    self.previous_angle = self.current_angle
    self.previous_pose = self.current_pose
    self.previous_dis_from_goal = self.current_dis_from_goal
    self.previous_left_dis = self.left_dis
    self.previous_right_dis = self.right_dis
    self.previous_front_dis = self.front_dis
    
    return
    
