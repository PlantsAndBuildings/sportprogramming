#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::ifstream;
using std::ios;
using std::endl;

// Number of buckets in the hash table.
// This could, as an alternate, be taken as user input. However, in order to achieve complete abstraction I have decided to use a global const.
const long int HT_SZ = 10;

// Hash Table Class

class HashTable{
	private:
		long int ht_sz;
		vector< vector <long int> > ht;
	public:
		HashTable(long int);
		long int get_hash_val(vector<long int>,long int);
		void add_to_table(vector<long int>,long int,long int);
		vector<long int> get_all_rows_with_hash_val(long int);
};

// Hash table function definitions

// Constructor -- initializes the hash table
HashTable::HashTable(long int ht_sz){
	this->ht_sz = ht_sz;
	this->ht.resize(ht_sz);
}

// get_hash_val: This function returns the hash value of a tuple given the tuple and join attribute index as inputs.
// Hash function is f(k) = k (mod N)
// Here, k is the join attribute value and N is the number of buckets in the hash table (given by HT_SZ constant).
long int HashTable::get_hash_val(vector<long int>  tuple, long int join_attribute_index){
	return tuple[join_attribute_index-1] % this->ht_sz;
}

// add_to_table: This function adds a given input tuple to the hash table.
// I have only stored the indices of the tuples in the hash table rather than entire tuples -- smaller memory usage, hence faster lookup.
void HashTable::add_to_table(vector <long int>  tuple, long int tuple_index, long int join_attribute_index){
	long int pos = this->get_hash_val(tuple,join_attribute_index);
	this->ht[pos].push_back(tuple_index);
}

// get_all_rows_with_hash_val: This function returns the tuple indices of all tuples having a specific hash value.
vector<long int> HashTable::get_all_rows_with_hash_val(long int hash_val){
	return this->ht[hash_val];
}

// Table class

class Table{
	private:
		char * data_file_name;	// Relations are stored in data files.
		string relation_name;	// The first line of a file containing a relation provides three pieces of information -- first, the relation name
		ifstream data_file;
		long int num_tuples;		// Second, the number of tuples (rows) in the relation
		long int num_attributes;	// Third, the number of attributes in the relation
						// The relation is specified from the second line onwards (in the relation data file).
		vector< vector <long int> > tuples;	// tuples[i][j] denotes the j th attribute of the i th tuple.
	public:
		Table(char *);
		Table(long int num_attributes);
		Table(long int num_attributes, long int num_tuples);
		long int get_num_tuples();
		void add_tuple(vector<long int>  tuple);
		long int get_element(int row_num, int col_num);
		long int get_num_attributes();
		vector<long int> get_row(int row_num);
		void print();
};

// This constructor function opens the relation data file and reads the data.
// This constructor only needs the name of data file (containing the relation) as input.
Table::Table(char * data_file_name){
	this->data_file_name = data_file_name;
	this->data_file.open(this->data_file_name,ios::in);	// Open the data file (containing the relation).
	this->data_file >> this->relation_name;			// Read the relation name (first piece of information).
	this->data_file >> this->num_attributes;		// Read the number of attriutes (second piece of information).
	this->data_file >> this->num_tuples;			// Read the number of tuples (third piece of information).
	this->tuples.resize(this->num_tuples);			
	for(long int i=0; i<this->num_tuples; i++){
		this->tuples[i].resize(this->num_attributes);
		for(long int j=0; j<this->num_attributes; j++){
			this->data_file >> this->tuples[i][j];	// Read the actual tuples themselves.
		}
	}
}

// This is another constructor that only partially initializes a table.
// This constructor is called to create the table which will be the result of the join operation.
Table::Table(long int num_attributes){
	this->num_attributes = num_attributes;
}

// Just an additional constructor that I thought I might need -- this is never used.
Table::Table(long int num_attributes, long int num_tuples){
	this->num_tuples = num_tuples;
	this->num_attributes = num_attributes;
	this->tuples.resize(num_tuples);
	for(long int i=0; i<this->num_tuples; i++){
		this->tuples[i].resize(this->num_attributes);
	}
}

// get_num_attributes: Accessor function to get the number of attributes of the relation (which is a privte variable num_attributes of class Table)
long int Table::get_num_attributes(){
	return this->num_attributes;
}

// add_tuple: This function adds a tuple (which is provided as input) to the table.
void Table::add_tuple(vector<long int> tuple){
	if(tuple.size() != this->num_attributes){
		cout<<"tuple length and number of attributes in table do not match!"<<endl;
		return;
	}
	this->tuples.push_back(tuple);
	this->num_tuples ++;
	return;
}

// get_element: Accessor function to get the attribute value at the specified row and column.
long int Table::get_element(int row_num, int col_num){
	return this->tuples[row_num-1][col_num-1];
}

// get_row: Accessor function to get the entire row (provided the row number as input).
vector <long int> Table::get_row(int row_num){
	return this->tuples[row_num-1];
}

// get_num_tuples: Accessor function to get the number of tuples in the table.
long int Table::get_num_tuples(){
	return this->num_tuples;
}

// print: Utility function to print the table.
void Table::print(){
	for(long int i = 0; i<this->num_tuples; i++){
		for(long int j=0; j<this->num_attributes; j++){
			cout<<this->tuples[i][j]<<" ";
		}
		cout<<endl;
	}
}

// Join function - this takes as input two tables and respective join attributes and returns a table which is the join of the two input tables on the specified join attributes.
// The function performs the following sequence of operations:
// 	1. Creates a new hash table and populates it with rows of the first table.
// 	2. Scans each row of the second table and tries to match it against rows from the first table having the same hash value. Recall that the hash table was already populated with rows of the first table, which makes lookup of rows having a particular hash value fast/efficient.
// 	3. Adds the matched tuples (after concatenation) to the resulting table.
Table * join(Table * t1, long int join_attr_1, Table * t2, long int join_attr_2){

	HashTable h(HT_SZ);		// Create the hash table.
	
	Table * result = new Table(t1->get_num_attributes() + t2->get_num_attributes());	// Create table to store the result

	for(long int i=1; i <= t1->get_num_tuples(); i++)
		h.add_to_table(t1->get_row(i),i,join_attr_1);	// Populate the hash table with rows of the first table.
	
	for(long int i=1; i <= t2->get_num_tuples(); i++){	// Scan rows of the second table one by one
		
		long int curr_hash_val = h.get_hash_val(t2->get_row(i),join_attr_2);	// Hash value of the current row of table 2
		
		vector <long int> possible_matches = h.get_all_rows_with_hash_val(curr_hash_val);	// Get all rows of table 1 having the same hash value as that of current row (of table 2). These are possible matches for the join.

		for(long int j=0; j<possible_matches.size(); j++){	// Scan through the possible matches.

			if(t1->get_element(possible_matches[j],join_attr_1) == t2->get_element(i,join_attr_2)){	// If the join attributes are equal
				vector <long int> res_tuple(t1->get_num_attributes() + t2->get_num_attributes());
				vector <long int> tuple1(t1->get_row(possible_matches[j]));
				vector <long int> tuple2(t2->get_row(i));
				
				// Code to concatenate the tuples
				for(long int k = 0; k < tuple1.size(); k++){
					res_tuple[k] = tuple1[k];
				}
				for(long int k = 0; k < tuple2.size(); k++){
					res_tuple[k+tuple1.size()] = tuple2[k];
				}
				result->add_tuple(res_tuple);	// Add the concatenated tuple to the result.
			}
		}

	}
	return result;
}

// Driver
int main(int argc, char ** argv){
	
	// Some error handling
	if(argc == 2 && (argv[1] == "-h" || argv[1] == "--help")){
		cout<<"Usage:"<<endl;
		cout<<"hash-join <data_file_1> <data_file_2>"<<endl;
		return 0;
	}
	if(argc != 3){
		cout<<"Use --help or -h option for help."<<endl;
		return 0;
	}
	
	// Create the tables on which the join is to be performed.
	Table * relation1 = new Table(argv[1]);
	Table * relation2 = new Table(argv[2]);

	// Print the tables to verify if they were read correctly.
	cout<<"Relation 1"<<endl;
	relation1->print();
	cout<<"Relation 2"<<endl;
	relation2->print();

	// Get the join attributes for both tables as interactive input.
	long int join_attr_1, join_attr_2;
	cout<<"Enter the joining attribute for relation 1 : ";
	cin>>join_attr_1;
	cout<<"Enter the joining attribute for relation 2 : ";
	cin>>join_attr_2;

	// Perform the join.
	Table * result = join(relation1, join_attr_1, relation2, join_attr_2);
	
	// Print the result of join
	cout<<"Result"<<endl;
	result->print();

	// Calculate and print join selectivity.
	// Join selectivity is defined as (number of tuples in the join)/(number of tuples in the Cartesian product of the relations).
	double join_selectivity = (double)(result->get_num_tuples()) / (double)(relation1->get_num_tuples() * relation2->get_num_tuples());
	cout<<"Join Selectivity: "<<join_selectivity<<endl;

	// All done!
	return 0;
}
