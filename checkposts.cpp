#include <iostream>
#include <vector>
#include <stack>

#define pb push_back

using namespace std;

typedef long long int ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

stack<ll> topoSort;

void dfsUtil(vvi & al, vector<bool> & vis, ll src){
	vis[src] = true;
//	cout<<src<<" ";
	for(ll i=0; i<al[src].size(); i++){
		if(!vis[al[src][i]]){
			dfsUtil(al,vis,al[src][i]);
		}
	}
	topoSort.push(src);
}

void dfsUtil2(vvi & al, vector<bool> & vis, ll src, vi & scc){
	vis[src] = true;
	scc.pb(src);
	for(ll i=0; i<al[src].size(); i++){
		if(!vis[al[src][i]]){
			dfsUtil2(al,vis,al[src][i],scc);
		}
	}
}

void dfs(vvi & al){
	ll n  = al.size();
	vector<bool> vis(n,false);
	for(ll i=0; i<n; i++){
		if(!vis[i]){
			dfsUtil(al,vis,i);
		}
	}
}

void dfs2(vvi & al, vi & cost){
	vector<bool> vis(al.size(),false);
	ll money = 0;
	ll num_ways = 1;
	while(!topoSort.empty()){
		ll x = topoSort.top();
		topoSort.pop();
		if(!vis[x]){
			vi scc;
			dfsUtil2(al,vis,x,scc);
//			cout<<endl;
			ll min  = 1000000009;
			ll min_cnt = 0;
			for(ll i=0; i<scc.size(); i++){
//				cout<<scc[i]<<" ";
				if(cost[scc[i]] < min){
					min = cost[scc[i]];
				}
			}
			for(ll i=0; i<scc.size(); i++){
				if(cost[scc[i]] == min) min_cnt++;
			}
//			cout<<"\n"<<min<<" "<<min_cnt<<"\n";
			money += min;
			num_ways *= min_cnt;
			num_ways %= 1000000007;
		}
	}
	cout<<money<<" "<<num_ways<<"\n";
}

int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	ll n,m;
	cin>>n;
	vvi al(n);
	vvi alt(n);
	vi cost(n,0);
	for(ll i=0; i<n; i++) cin>>cost[i];
	cin>>m;
	for(ll i=0; i<m; i++){
		ll tmp1, tmp2;
		cin>>tmp1>>tmp2;
		tmp1--; tmp2--;
		al[tmp1].pb(tmp2);
		alt[tmp2].pb(tmp1);
	}
//	cout<<"ignore these\n";
	dfs(al);
//	cout<<"SCCs:\n";
	dfs2(alt,cost);
	return 0;
}
