//http://codeforces.com/problemset/problem/731/C

/*
 * This code is awesome
 * Wednesday 26 October 2016 10:53:07 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <map>

#define fs first
#define sc second
#define all(c) c.begin(),c.end()

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

const ll inf = 1000000000+9;
ll n,m,k;
vi c;
ll sz = 0;
ll maximum = -1;

class Graph{
	private:
		//vvi adjList;
		map<int, set<int> > adjList;
		ll E, V;
		ll cc_num;

	public:
		Graph(ll V);
		void addEdge(ll u, ll v);
		ll DFS_Util(ll source, vector<bool> & vis, map<ll,ll> & mapp);
		ll DFS();
};

Graph::Graph(ll V){
	this->V = V;
	//this->adjList.resize(V);
	this->E = 0;
	this->cc_num = 0;
}

void Graph::addEdge(ll u, ll v){
	this->E += 1;
	//this->adjList[u].push_back(v);
	//this->adjList[v].push_back(u);
	adjList[u].insert(v);
	adjList[v].insert(u);
	return;
}

ll Graph::DFS_Util(ll source, vector<bool> & vis, map<ll,ll> & colors){
	vis[source] = true;
	colors[c[source]]++;
	//nums[c[source]-1]++;
	if(maximum < colors[c[source]]) maximum = colors[c[source]];
	sz++;
	for(set<int>::iterator setIter = adjList[source].begin(); setIter != adjList[source].end(); setIter++){
		if(!vis[*setIter]){
			this->DFS_Util(*setIter,vis,colors);
		}
	}
	/*
	for(ll i=0; i<this->adjList[source].size(); i++){
		if(!vis[this->adjList[source][i]]){
			this->DFS_Util(this->adjList[source][i], vis, nums);
		}
	}
	*/
	return maximum;
}

ll Graph::DFS(){
	vector<bool> vis(this->V,false);
	ll ans = 0;
	for(ll i=0; i<this->V; i++){
		if(!vis[i]){
			this->cc_num++;
			//vi nums(k,0);
			//vi colors;
			map<ll,ll> colors;
			sz = 0;
			maximum = -1;
			maximum = this->DFS_Util(i,vis,colors);
			//sort(colors.begin(), colors.end());

			/*
			for(ll i=0; i<nums.size(); i++){
				if(nums[i] > maximum) maximum = nums[i];
			}
			*/
			ans += sz-maximum;
		}
	}
	return ans;
}

int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	cin>>n>>m>>k;
	c.resize(n,0);
	Graph g(n);
	for(ll i=0; i<n; i++) cin>>c[i];
	for(ll i=0; i<m; i++){
		ll l,r;
		cin>>l>>r;
		l--; r--;
		g.addEdge(l,r);
	}
	cout<<g.DFS()<<"\n";
	return 0;
}
