/*
 * This code is awesome
 * Saturday 10 September 2016 11:42:20 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <map>

#define fs first
#define sc second
#define all(c) c.begin(),c.end()

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

const ll inf = 1000000000+9;

typedef struct triangle{
	ll a,b,c;
	ll time;
}triangle;

bool equal(triangle * t1, triangle * t2){
	if(t1->a == t2->a && t1->b == t2->b && t1->c ==t2->c){
		return true;
	}
	return false;
}

bool isT(ll a, ll b, ll c){
	vi arr(3,0);
	arr[0] = a;
	arr[1] = b;
	arr[2] = c;
	sort(all(arr));
	if((arr[0]+arr[1])>arr[2])return true;
	return false;
}

ll bfs(triangle * s, triangle * fin){
	queue<triangle *> q;
	q.push(s);
	map<triangle*,bool> vis;
	while(!q.empty()){
		triangle * curr = q.front();
		vis[curr] = true;
		q.pop();
		ll tmp = 1;
		if(equal(curr,fin)){
			return curr->time;
		}
		while(isT(curr->a,curr->b,curr->c-tmp)){
			triangle * n = (triangle*)malloc(sizeof(triangle));
			n->a = curr->a;
			n->b = curr->b;
			n->c = curr->c-tmp;
			n->time = curr->time+1;
			if(!vis[n]) q.push(n);
			else free(n);
			tmp++;
		}
		tmp = 1;
		while(isT(curr->a,curr->b-tmp,curr->c)){
			triangle * n = (triangle*)malloc(sizeof(triangle));
			n->a = curr->a;
			n->b = curr->b-tmp;
			n->c = curr->c;
			n->time = curr->time+1;
			if(!vis[n]) q.push(n);
			else free(n);
			tmp++;
		}
		tmp = 1;
		while(isT(curr->a-tmp,curr->b,curr->c)){
			triangle * n = (triangle*)malloc(sizeof(triangle));
			n->a = curr->a-tmp;
			n->b = curr->b;
			n->c = curr->c;
			n->time = curr->time+1;
			if(!vis[n]) q.push(n);
			else free(n);
			tmp++;
		}
	}
}

int main(int argc, char ** argv){
	ll x;
	ll y;
	cin>>x>>y;
	triangle * init = (triangle*)malloc(sizeof(triangle));
	init->a = x;
	init->b = x;
	init->c = x;
	init->time = 0;
	triangle * fin = (triangle*)malloc(sizeof(triangle));
	fin->a = y;
	fin->b = y;
	fin->c = y;	
	cout<<bfs(init,fin)<<"\n";
	return 0;
}
