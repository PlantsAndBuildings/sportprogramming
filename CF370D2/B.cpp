/*
 * This code is awesome
 * Saturday 10 September 2016 10:33:59 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <map>

#define fs first
#define sc second
#define all(c) c.begin(),c.end()

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

const ll inf = 1000000000+9;

ll f(ll n1, ll n2){
	if(n1>n2) return n1-n2;
	return 0;
}

int main(int argc, char ** argv){
	string s;
	cin>>s;
	ll l=0,r=0,u=0,d=0;
	for(int i=0; i<s.length(); i++){
		if(s[i] == 'U'){
			u++;
		}else if(s[i] == 'D'){
			d++;
		}else if(s[i] == 'L'){
			l++;
		}else if(s[i] == 'R'){
			r++;
		}
	}
	if(((u+l+r+d)%2)!=0){
		cout<<"-1\n";
		return 0;
	}
	ll w = (u+l+r+d)/2;
	ll x;
	ll ans = inf;
	for(x = 0; x<=w; x++){
		ans = min(ans,f(u,x)+f(d,x)+f(l,w-x)+f(r,w-x));
	}
	cout<<ans<<"\n";
	return 0;
}
