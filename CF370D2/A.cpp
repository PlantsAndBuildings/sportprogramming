/*
 * This code is awesome
 * Saturday 10 September 2016 10:26:31 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <map>

#define fs first
#define sc second
#define all(c) c.begin(),c.end()

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

const ll inf = 1000000000+9;

int main(int argc, char ** argv){
	ll n;
	cin>>n;
	vi a(n,0);
	vi b(n,0);
	for(int i=0; i<n; i++) cin>>a[i];
	for(int i=1; i<n; i++) b[i-1] = a[i]+a[i-1];
	b[n-1] = a[n-1];
	for(int i=0; i<n; i++) cout<<b[i]<<" ";
	cout<<"\n";
	return 0;
}
