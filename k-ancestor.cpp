//https://www.hackerrank.com/challenges/kth-ancestor

#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

typedef long long ll;

typedef struct TreeNode{
	ll data;
	struct TreeNode * parent;
	TreeNode ** ancestor; 
}TreeNode;

TreeNode * getNode(ll data){
	TreeNode * ret = (TreeNode*)malloc(sizeof(TreeNode));
	ret->data = data;
	ret->parent = NULL;
	ret->ancestor = (TreeNode **) malloc(18*sizeof(TreeNode*));
	for(int i=0; i<18; i++){
		ret->ancestor[i] = NULL;
	}
	return ret;
}

int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	ll t;
	cin>>t;
	while(t--){
		TreeNode * ref[100001];
		for(ll i=0; i<100001; i++){
			ref[i] = NULL;
		}
		ll p,x,y,k;
		cin>>p;
		for(ll i=0; i<p; i++){
			cin>>x>>y;		// y is the parent of x
			if(ref[y] == NULL && y != 0){
				ref[y] = getNode(y);
			}
			if(ref[x] == NULL){
				ref[x] = getNode(x);
			}
			ref[x]->parent = ref[y];
			ref[x]->ancestor[0] = ref[y];
		}
		for(int i=1; i<18; i++){
			for(int j=0; j<100001; j++){
				if(ref[j] != NULL){
					TreeNode * tmp = ref[j]->ancestor[i-1];
					if(tmp != NULL){
						ref[j]->ancestor[i] = tmp->ancestor[i-1];	
					}
				}
			}
		}
		ll num_q;
		cin>>num_q;
		while(num_q--){
			ll q;
			cin>>q;
			if(q==0){
				cin>>y>>x;
				if(ref[x] == NULL){
					ref[x] = getNode(x);
				}
				ref[x]->parent = ref[y];
				ref[x]->ancestor[0] = ref[y];
				for(int i=1; i<18; i++){
					TreeNode * tmp = ref[x]->ancestor[i-1];
					if(tmp != NULL){
						ref[x]->ancestor[i] = tmp->ancestor[i-1];
					}
				}
			}else if(q==1){
				cin>>x;
				free(ref[x]);
				ref[x] = NULL;
			}else if(q==2){
				cin>>x>>k;
				ll cnt = 0;
				TreeNode * curr = ref[x];
				if(curr == NULL){
					cout<<"0\n";
				}else{
					while(k!=0){
						ll tmp = k%2;
						k = k/2;
						if(tmp == 1){
							curr = curr->ancestor[cnt];
							if(curr == NULL){
								cout<<"0\n";
								break;
							}
						}
						cnt++;
					}
					if(curr != NULL){
						cout<<(curr->data)<<"\n";
					}
				}
			}else{
				cout<<"Life's Good, Isn't it?\nTerminating...\n";
				return 0;
			}
		}
	}
	return 0;
}
