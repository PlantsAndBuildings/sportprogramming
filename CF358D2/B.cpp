#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ll n;
	cin>>n;
	vi a(n,0);
	for(int i=0; i<n; i++){
		cin>>a[i];
	}
	sort(a.begin(), a.end());
	for(int i=0; i<n; i++){
		if(i == 0){
			if(a[i] > 1) a[i] = 1;
		}else{
			if(a[i] > a[i-1]+1) a[i] = a[i-1]+1;
		}
	}
	cout<<(a[n-1]+1)<<"\n";
	return 0;
}
