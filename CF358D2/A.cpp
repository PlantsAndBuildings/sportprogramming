#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long int ll;
typedef unsigned long long int ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ll n,m;
	cin>>n>>m;
	vi f(5,0), g(5,0);
	for(int i=1; i<=n; i++){
		f[i%5]++;
	}
	for(int i=1; i<=m; i++){
		g[i%5]++;
	}
	ll ans = 0;
	for(int i=1;i<5;i++){
		ans += f[i]*g[5-i];
	}
	ans += f[0]*g[0];
	cout<<ans<<"\n";
	return 0;
}
