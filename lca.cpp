// http://www.spoj.com/problems/LCASQ/
// Nice problem.. pretty standard though.
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

// log2(ht)
ll getNode(vvi & st, ll u, ll ht){
    if(ht == 0) return u;
    if(ht == 1) return st[u][0];
    else{
        ll i = 0;
        while((1<<i)<ht) i++;
        if((1<<i) == ht) return st[u][i];
        else{
            return getNode(st,st[u][i-1],ht-(1<<(i-1)));
        }
    }
}

// precondition u and v are on the same level
// log(lev[u])
ll findLCA(vvi & st, ll u, ll v){
    if(u == v) return u;
    while(!(st[u][0] == st[v][0])){
        ll i=0;
        while(st[u][i] != st[v][i]) i++;
        u = st[u][i-1];
        v = st[v][i-1];
    }
    return st[u][0];
}

int main(int argc, char ** argv){
    ll n;
    cin>>n;
    vvi st(n,vi(20,-1));
    vvi al(n);
    for(ll i=0; i<n; i++){
        ll m,tmp;
        cin>>m;
        for(ll j=0; j<m; j++){
            cin>>tmp;
            al[i].push_back(tmp);
            st[tmp][0] = i;
        }
    }
    for(ll i=1; i<20; i++){
        for(ll j=0; j<n; j++){
            if(st[j][i-1] != -1) st[j][i] = st[ st[j][i-1] ][ i-1 ];
        }
    }
    vi lev(n,0);
    queue<ll> Q;
    vector<bool> visited(n,false);
    Q.push(0);
    while(!Q.empty()){
        ll curr = Q.front();
        ll curr_lev = lev[curr];
        visited[curr] = true;
        Q.pop();
        for(ll i=0; i<al[curr].size(); i++){
            if(!visited[al[curr][i]]){
                lev[al[curr][i]] = curr_lev+1;
                Q.push(al[curr][i]);
            }
        }
    }
    ll q;
    cin>>q;
    for(ll i=0; i<q; i++){
        ll u,v;
        cin>>u>>v;
        if(lev[u] == lev[v]){
            ll ans = findLCA(st,u,v);
	    cout<<ans<<"\n";
        }else if(lev[u] < lev[v]){
            ll x = getNode(st,v,lev[v]-lev[u]);
            ll ans = findLCA(st,x,u);
            cout<<ans<<"\n";
        }else if(lev[u] > lev[v]){
            ll x = getNode(st,u,lev[u]-lev[v]);
            ll ans = findLCA(st,x,v);
            cout<<ans<<"\n";
        }
    }
    return 0;
}
