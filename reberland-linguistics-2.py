# http://codeforces.com/contest/666/problem/A
# This TLEs after 17 test cases
from sets import Set

s = raw_input()

if len(s) < 7:
    print('0')
    exit(0)

dp1 = [Set() for i in range(len(s))]
dp2 = [Set() for i in range(len(s))]

for i in range(6,len(s)):
    if i == 6:
        dp1[i].update([s[i-1:i+1]])
    elif i == 7:
        dp1[i].update([s[i-1:i+1]])
        dp2[i].update([s[i-2:i+1]]) 
    else:
        if s[i-1:i+1] != s[i-3:i-1]:
            if i-1 >= 5:
                dp1[i].update(dp1[i-2]|Set([s[i-1:i+1]])|dp2[i-2])
        else:
            if i-1 >= 5:
                dp1[i].update(Set([s[i-1:i+1]])|dp2[i-2])
        if s[i-2:i+1] != s[i-5:i-2]:
            if i-2 >= 5:
                dp2[i].update(dp2[i-3]|Set([s[i-2:i+1]])|dp1[i-3])
        else:
            if i-2 >= 5:
                dp2[i].update(Set([s[i-2:i+1]])|dp1[i-3])

ans = list(Set(dp1[len(s)-1]|dp2[len(s)-1]))
ans.sort()
print(len(ans))
for suffix in ans:
    print suffix

