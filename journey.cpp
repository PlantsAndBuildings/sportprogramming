//http://codeforces.com/problemset/problem/721/C
/*
 * This code is awesome
 * Thursday 27 October 2016 08:44:05 PM IST
 */


#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <fstream>
#include <map>

#define fs first
#define sc second
#define all(c) c.begin(),c.end()

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

const ll inf = 1000000000+9;
ll T;
ll ans = -inf;
stack<ll> best;

class Graph{
	private:
		map<ll, set<ii> > adjList;
		ll V, E;
		ll cc_num;
	public:
		Graph(ll);
		void addEdge(ll,ll,ll);
		void DFS();
		void DFSUtil(ll source, vector<bool> & vis, ll dis, ll num_vis, vi & par);
};

Graph::Graph(ll V){
	this->V = V;
	this->E = 0;
	this->cc_num = 0;
}

void Graph::addEdge(ll u ,ll v, ll t){
	adjList[u].insert(ii(v,t));
	(this->E)++;
}

void Graph::DFS(){
	vector<bool> vis(this->V, false);
	for(int i=0; i<this->V; i++){
		if(!vis[i]){
			//this->DFSUtil(i,vis);
		}
	}
}


void Graph::DFSUtil(ll source, vector<bool>& vis, ll dis, ll num_vis, vi & par){
	vis[source] = true;
	//ll tmp = -inf;
	for(set<ii>::iterator setIter = this->adjList[source].begin(); setIter != this->adjList[source].end(); setIter++){
		if(!vis[setIter->first]){
			par[setIter->first] = source;
			if(setIter->first == vis.size()-1){
				if(dis+setIter->second <= T){
					//ans = max(ans,num_vis+1);	
					
					if(num_vis+1 > ans){
						ans = num_vis+1;
						while(!best.empty()) best.pop();
						best.push(setIter->first);
						ll curr = setIter->first;
						while(par[curr]!=-1){
							best.push(par[curr]);
							curr = par[curr];
						}
						//return dis+setIter->second;
					}else{
						//return ans;
					}
					
				}
			}else{
				this->DFSUtil(setIter->first,vis,dis+setIter->second,num_vis+1,par);
			}
		}
	}
}


int main(int argc, char ** argv){
	fstream fin("input.txt",ios::in);
	fstream fout("output.txt",ios::out);
	ll n, m;
	fin>>n>>m>>T;
	Graph g(n);
	for(ll i=0; i<m; i++){
		ll u, v, t;
		fin>>u>>v>>t;
		u--; v--;
		g.addEdge(u,v,t);
	}
	vector<bool> vis(n,false);
	vi par(n,0);
	par[0] = -1;
	g.DFSUtil(0,vis,0,0,par);
	fout<<(ans+1)<<"\n";
	while(!best.empty()){
		fout<<(best.top()+1)<<" ";
		best.pop();
	}
	return 0;
}
