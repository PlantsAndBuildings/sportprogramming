MAX = 823543
n,m = map(int,raw_input().split())
if n > MAX or m > MAX:
	print 0
else:
	num_dg_hr = 1		# Smallest power of 7 greater than n
	num_dg_min = 1		# Smallest power of 7 greater than m
	while int((n-1)/(7**num_dg_hr)) != 0:
		num_dg_hr += 1
	while int((m-1)/(7**num_dg_min)) != 0:
		num_dg_min += 1
	print "num_dg_hr = "+str(num_dg_hr)
	print "num_dig_min = "+str(num_dg_min)
	if num_dg_hr + num_dg_min > 7:
		print 0
	else:
		ans  = 0
		for i in range(n*m):
			hrs = i/m
			min = i%m
			time = [0 for j in range(num_dg_hr+num_dg_min)]
			for j in range(num_dg_min):
				time[j] = min%7
				min = int(min/7)
			for j in range(num_dg_min,num_dg_min+num_dg_hr):
				time[j] = hrs%7
				hrs = int(hrs/7)
			cnt = [0 for j in range(7)]
			for j in range(num_dg_min+num_dg_hr):
				cnt[time[j]] += 1
			are_nums_distinct = True
			for j in range(7):
				if cnt[j] > 1:
					are_nums_distinct = False
			if are_nums_distinct:
				ans += 1
		print ans
