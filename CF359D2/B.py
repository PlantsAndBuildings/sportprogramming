n = int(raw_input())
a = map(int,raw_input().split())
ans = 0
for i in xrange(n):
	min = i
	for j in xrange(i+1,n):
		if a[j] < a[min]:
			min = j
	tmp = a[min]
	for j in range(min,i,-1):
		a[j] = a[j-1]
		print " ".join((str(j),str(j+1)))
	a[i] = tmp
	ans += min-i
