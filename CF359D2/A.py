n,x = map(int,raw_input().split())
dis = 0
for i in range(n):
	ch,num = raw_input().split()
	if ch == '+':
		x += int(num)
	elif ch == '-':
		if x >= int(num):
			x -= int(num)
		else:
			dis += 1
print " ".join((str(x),str(dis)))
