#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef long long ll;

int main(int argc, char ** argv){
	
	ll MAX = 823543;
	ll n,m;
	scanf("%lld%lld",&n,&m);
	if(n>MAX || m>MAX){
		printf("0\n");
		return 0;
	}
	ll num_dg_hr = 1;
	ll num_dg_min = 1;
	ll x1 = 7*num_dg_hr;
	ll x2 = 7*num_dg_min;
	while((n-1)/x1!=0){
		num_dg_hr++;
		x1*=7;
	}
	while((m-1)/x2!=0){
		num_dg_min++;
		x2*=7;
	}
	if(num_dg_min+num_dg_hr>7){
		printf("0\n");
		return 0;
	}
	ll i,j;
	ll ans = 0;
	for(i=0; i<n*m; i++){
		ll hr = i/m;
		ll min = i%m;
		ll * time = (ll*)calloc(sizeof(ll),(num_dg_min+num_dg_hr));
		for(j=0; j<num_dg_min; j++){
			time[j] = min%7;
			min = min/7;
		}
		for(j=num_dg_min; j<num_dg_min+num_dg_hr; j++){
			time[j] = hr%7;
			hr = hr/7;
		}
		ll * cnt = (ll*)calloc(sizeof(ll),7);
		for(j=0; j<num_dg_min+num_dg_hr; j++){
			cnt[time[j]]++;
		}
		bool flag = true;
		for(j=0; j<7; j++){
			if(cnt[j]>1){
				flag = false;
			}
		}
		if(flag){
			ans++;
		}
	}
	printf("%lld\n",ans);

	return 0;
}
