//www.codechef.com/problems/FNCS
//This code passes the first subtask
#include <iostream>
#include <vector>
#include <fstream>
#include <set>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ull> vi;
typedef vector<vi> vvi;
typedef pair<ull,ull> ii;
typedef vector<ii> vii;

const int M = 100000;
const int BKT_SZ = 320;

int main(int argc, char ** argv){
	//ios_base::sync_with_stdio(false);
	//fstream fin, fout;
	//fin.open("in",ios::in);
	//fout.open("out",ios::out);
	ull n;
	//cin>>n;
	//fin>>n;
	scanf("%lld",&n);	
	vi a(M+5,0);
	vi fn(M+5,0);
	vi fnl(M+5,0), fnr(M+5,0);
	vi bkt1(BKT_SZ,0);
	for(ull i =0;i<n;i++){
		//cin>>a[i];
		//fin>>a[i];
		scanf("%lld",&a[i]);
	}
	for(ull i=0; i<n;i++){
		bkt1[(ull)(i/BKT_SZ)] += a[i]; 
	}
	for(ull i=0;i<n;i++){
		ull l,r;
		//cin>>l>>r;
		//fin>>l>>r;
		scanf("%lld%lld",&l,&r);
		l--;
		r--;
		fnl[i] = l;
		fnr[i] = r;
		ull x = l/BKT_SZ;
		ull y = r/BKT_SZ;
		if(x==y){
			for(ull j=l;j<(x+1)*BKT_SZ && j<=r;j++) fn[i]+=a[j];
		}else{
			for(ull j=x+1;j<y;j++) fn[i] += bkt1[j];
			for(ull j=l;j<(x+1)*BKT_SZ && j<=r;j++) fn[i]+=a[j];
			for(ull j=y*BKT_SZ;j<=r && j>=l;j++) fn[i]+=a[j];
		}
	}
	//for(ull i=0; i<n; i++) fout<<"f["<<i<<"] = "<<fn[i]<<"\n";
	vi bkt2(BKT_SZ,0);
	for(ull i=0;i<n;i++){
		bkt2[(ull)(i/BKT_SZ)] += fn[i];
	}
	ull q;
	//does it make a difference if i use vector<set> instead?
	vvi fnbk(BKT_SZ);
	for(ull i =0;i<BKT_SZ;i++){
		for(ull j =0;j<n;j++){
			if(fnl[j]<=(i+1)*BKT_SZ-1 && fnr[j]>=i*BKT_SZ)
				fnbk[i].push_back(j);
		}
	}
	//cin>>q;
	//fin>>q;
	scanf("%lld",&q);
	while(q--){
		ull qt;
		//cin>>qt;
		//fin>>qt;
		scanf("%lld",&qt);
		if(qt==1){
			ull x,y;
			//cin>>x>>y;
			//fin>>x>>y;
			scanf("%lld%lld",&x,&y);			
			x--;
			ull tmp = a[x];
			a[x] = y;
			bkt1[(ull)(x/BKT_SZ)] = bkt1[(ull)(x/BKT_SZ)]+y-tmp;
			for(ull i=0;i<fnbk[(ull)(x/BKT_SZ)].size();i++){
				ull fn_no = fnbk[(ull)(x/BKT_SZ)][i];
				if(x<=fnr[fn_no] && x>=fnl[fn_no]){
					ull tmp2 = fn[fn_no];
					fn[fn_no] = fn[fn_no]+y-tmp;
					bkt2[(ull)(fn_no/BKT_SZ)] -= tmp;
					bkt2[(ull)(fn_no/BKT_SZ)] += fn[fn_no];
				}
			}
			//for(ull i=0; i<n; i++) fout<<"f["<<i<<"] = "<<fn[i]<<"\n";
		}else if(qt==2){
			ull ans =0,l,r,x,y;
			//cin>>l>>r;
			//fin>>l>>r;
			scanf("%lld%lld",&l,&r);			
			l--; r--;
			x = l/BKT_SZ;
			y = r/BKT_SZ;
			if(x==y){
				for(ull j=l;j<(x+1)*BKT_SZ && j<=r;j++) ans+=fn[j];
			}else{
				for(ull j=x+1;j<y;j++) ans += bkt2[j];
				for(ull j=l;j<(x+1)*BKT_SZ && j<=r;j++) ans+=fn[j];
				for(ull j=y*BKT_SZ;j<=r && j>=l;j++) ans+=fn[j];
			}
			//cout<<ans<<"\n";
			//fout<<ans<<"\n";
			printf("%lld\n",ans);
		}
	}
	return 0;
}
