// Easy porblem -- standard problem.. Used BFS
// http://www.spoj.com/problems/STAR3CAS/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;
typedef pair<ll,ll> ii;
typedef queue<ll> qi;

int main(int argc, char ** argv){
    ll t;
    cin>>t;
    while(t--){
        ll n;
        cin>>n;
        vector<bool> visited(n+1,false);
        vi x(n,0);
        for(ll i=0; i<n; i++) cin>>x[i];
        ll curr = 0, curr_lev = 0;
        queue< pair<ll,ll> > Q;
        Q.push(ii(curr,curr_lev));
        while(!Q.empty()){   
            curr = Q.front().first;
            curr_lev = Q.front().second;
            Q.pop();
            visited[curr] = true;
            if(curr == n) break;
            else{
                if(!visited[curr+1]) Q.push(ii(curr+1,curr_lev+1));
                if( (curr+x[curr])<=n && (curr+x[curr])>=0 && !visited[curr+x[curr]]) Q.push(ii(curr+x[curr],curr_lev+1));
            }
        }
        if(curr == n) cout<<curr_lev<<"\n";
        else cout<<"-1\n";
    }
    return 0;
}
