//codeforces.com/problemset/problem/189/A
#include <iostream>
#include <cstdio>
#include <vector>
#include <cstdlib>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

ll intmax = 1000000000+9;


int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	ll n,a,b,c;
	cin>>n>>a>>b>>c;
	vvi dp(3,vi(4005,0-intmax));
	for(int i=0 ; i<=n; i++){
		if(i%a == 0) dp[0][i] = i/a;
	}
	for(int i=0 ; i<=n; i++){
		ll k = i/b;
		for(int j=0; i-j*b>=0; j++){
			if(i-j*b>=0){
				dp[1][i] = max(dp[1][i],dp[0][i-(j*b)]+j);
			}
		}
	//	if(dp[1][i]>=0) cout<<dp[1][i]<<" ";
	}
	//cout<<"\n";
	for(int i=0; i<=n; i++){
		ll k = i/c;
		for(int j=0; i-j*c >= 0;j++){
			if(i-j*c>=0){
				dp[2][i] = max(dp[2][i],dp[1][i-(j*c)]+j);
			}
		}
	//	if(dp[2][i]>=0) cout<<i<<"-"<<dp[2][i]<<" ";
	}
	//cout<<"\n";
	cout<<dp[2][n]<<"\n";
	return 0;
}
