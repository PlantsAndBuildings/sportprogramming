//codeforces.com/problemset/problem/455/A
#include <iostream>
#include <vector>

using namespace std;

typedef long long int ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ios_base::sync_with_stdio(false);
	ll n;
	cin>>n;
	vi a(n,0), dp(100005,0), cnt(100005,0);
	for(int i=0;i<n;i++){
		cin>>a[i];
		cnt[a[i]]++;
	}
	dp[1] = cnt[1];
	dp[2] = max(cnt[2]*2,cnt[1]);
	for(int i=3;i<=100000;i++){
		dp[i] = max(dp[i-1],dp[i-2]+cnt[i]*i);
	}
	cout<<dp[100000]<<"\n";
	return 0;
}
