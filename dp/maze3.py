'''
http://codeforces.com/problemset/problem/407/B
Finally AC.
'''
n = int(raw_input())
p = [0 for i in range(n+1)]
p[1:] = map(int,raw_input().split())
crosses = [0 for i in range(n+5)]
dp = [0 for i in range(n+1)]
dp[1] = 2
presum = [0 for i in range(n+1)]
for i in range(2,n+1):
    for j in range(p[i],i):
        dp[i] += dp[j]
    dp[i]+=2
res = 0
for i in range(n+1):
    res += dp[i]
    res %= 1000000000+7
print(res)
