'''
http://codeforces.com/problemset/problem/407/B
Better iterative version
Still TLE
'''
n = int(raw_input())
p = map(int,raw_input().split())
crosses = [0 for i in range(n+5)]
crosses[1] = 1
curr = 1
while curr != (n+1):
    #print(curr)
    if crosses[curr]%2 == 0:
        crosses[curr+1] += 1
        curr += 1
    elif crosses[curr]%2 == 1:
        crosses[p[curr-1]] += 1
        curr = p[curr-1]
res = 0
for cross in crosses:
    res += cross
    res %= 1000000007
print(res-1)
