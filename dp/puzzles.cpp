//codeforces.com/problemset/problem/337/A
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;


typedef long long int ll;
typedef unsigned long long ull;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;

int main(int argc, char ** argv){
	ll n,m;
	cin>>n>>m;
	vi f(m,0);
	for(int i=0;i<m;i++) cin>>f[i];
	sort(f.begin(),f.end());
//	for(int i=0;i<m;i++) cout<<f[i];
	ll i =0;
	ll min = 100000000;
	while(i+n-1 <= m-1){
		if(f[i+n-1] - f[i] < min) min = f[i+n-1]-f[i];
		i++;
	}
	cout<<min<<"\n";
	return 0;
}
