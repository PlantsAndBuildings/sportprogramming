//codeforces.com/problemset/problem/474/D
#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;

const int MOD = 1000000000+7;

int main(int argc, char ** argv){
	//dp[n] = dp[n-1]+dp[n-k]
	ll t,k,a,b;
	cin>>t>>k;
	vi dp(100005,0);
	for(int i=0;i<=k-1;i++) dp[i] = 1;
	dp[k] = 2;
	for(int i = k+1;i<=100000;i++) dp[i] = (dp[i-1]+dp[i-k])%MOD;
	for(int i=1;i<=100000; i++){
		dp[i] = (dp[i]+dp[i-1])%MOD;
	}
	while(t--){
		cin>>a>>b;
		//cout<<(dp[b]-dp[a-1])<<"\n";
		if(dp[b]-dp[a-1]<0) cout<<(dp[b]-dp[a-1]+MOD)<<"\n";
		else cout<<dp[b]-dp[a-1]<<"\n";
	}
	return 0;
}
