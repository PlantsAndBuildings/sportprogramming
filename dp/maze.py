'''
http://codeforces.com/problemset/problem/407/B
Recursive Solution
Correct but not efficient. TLE.
'''
def rec(pty,i,j,crosses,p):
    if i == j :
        return 0
    if  pty == 0 and j==i+1:
        crosses[i+1] = crosses[i+1]+1    
        return 1
    elif pty == 0 and j != i+1:
        crosses[i+1] = crosses[i+1]+1
        return (rec(crosses[i+1]%2,i+1,j,crosses,p)%(1000000007)+1)%(1000000007)
    elif pty == 1:
        crosses[p[i]] = crosses[p[i]]+1
        return (rec(crosses[p[i]]%2,p[i],i,crosses,p)%(1000000007)+rec(0,i,j,crosses,p)%(1000000007)+1)%(1000000007)

for i in range(n):
    p[i] = p[i]-1
print(rec(1,0,n,crosses,p)%(1000000007))
