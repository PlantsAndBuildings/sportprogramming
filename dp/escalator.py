'''
codeforces.com/problemset/problem/518/D
'''
inp = raw_input().split()
n,tm = int(inp[0]),int(inp[2])
p = float(inp[1])
dp = [[0.0 for j in range(2005)]for i in range(2005)]
dp[0][1] = 1-p
dp[1][1] = p
for i in xrange(2,2001):
    dp[0][i] =  dp[0][i-1]*(1-p)
for i in xrange(2,n+1):
    dp[i][i] = dp[i-1][i-1]*p


for k in xrange(1,2001):
    for t in xrange(k+1,2001):
        if k<n:
            dp[k][t] = dp[k-1][t-1]*p + dp[k][t-1]*(1-p)
        elif k==n:
            dp[k][t] = dp[k-1][t-1]*p + dp[k][t-1]
'''
for i in range(15):
    print(dp[i])
'''
exp = 0.0
for i in range(1,2001):
    exp += i*dp[i][tm]

print(exp)
