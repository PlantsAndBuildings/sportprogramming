//codeforces.com/problemset/problem/363/B
#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;
typedef vector<ll> vi;

int main(int argc, char ** argv){
	ll n,k;
	cin>>n>>k;
	vi h(n,0);
	vi presum(n,0);
	for(int i=0;i<n;i++){
		cin>>h[i];
		if(i==0) presum[i] = h[i];
		else presum[i] = presum[i-1]+h[i];
	}
	ll min = 1000000000;
	ll minj = -1;
	for(int i=0;(i+k-1)<n;i++){
		if((presum[i+k-1]-presum[i]+h[i]) < min){
			min = presum[i+k-1]-presum[i]+h[i];
			minj = i;
		}
	}
	cout<<(minj+1)<<"\n";
	return 0;
}
